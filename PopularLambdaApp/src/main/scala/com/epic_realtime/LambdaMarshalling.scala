package com.epic_realtime

import spray.json._

case class QueryDescription(lowerTime: String="", upperTime: String="", trendType: String ="", value: String ="") 

case class MassageEntry(msg: String) 

case class Error(message: String) 

trait LambdaMarshalling extends DefaultJsonProtocol {
  import LambdaSupervisor._
  import ServingLayerSupervisor._
  
  implicit val trendLambdaFormat = jsonFormat7(LambdaSupervisor.TrendLambda)
  implicit val trendLambdasFormat = jsonFormat1(LambdaSupervisor.TrendLambdas)
  
  implicit val trendQueryDescriptionFormat = jsonFormat4(QueryDescription)
  implicit val trendQueryFormat = jsonFormat10(ServingLayerSupervisor.TrendQuery)
  implicit val trendQueriesFormat = jsonFormat1(ServingLayerSupervisor.TrendQueries)

  implicit val popularQueryResultFormat = jsonFormat4(ServingLayerSupervisor.PopularQueryResult)

  implicit val massageEntryFormat = jsonFormat1(MassageEntry)
  implicit val errorFormat = jsonFormat1(Error)

 }
