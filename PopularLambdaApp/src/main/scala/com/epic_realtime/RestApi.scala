package com.epic_realtime

import scala.concurrent.duration._
import scala.concurrent.ExecutionContext
import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global

import akka.actor._
import akka.pattern.ask
import akka.util.Timeout

import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport._
import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server._

import com.epic_realtime.utils.CassandraClient

import scala.concurrent.{Await, Future}
import scala.concurrent.duration._

case class EventQuery(event: String, attribute: String, id: String)


class RestApi(system: ActorSystem, timeout: Timeout) extends RestRoutes {
  implicit val requestTimeout = timeout
  implicit def executionContext = system.dispatcher

  def createLambdaSupervisor = system.actorOf(LambdaSupervisor.props, LambdaSupervisor.name)
  def selectServingLayer(event: String, atrribute: String) =
    system.actorSelection(s"/user/lambdaSupervisor/${event}_lambda/${event}_lambda_${atrribute}_serving")//.resolveOne()

  // def initialize() = {
  //   val cassandraClient = new CassandraClient("localhost")
  //   cassandraClient.createSchema
  //   cassandraClient.close
  // }

}

trait RestRoutes extends LambdaSupervisorApi
    with LambdaMarshalling {
  import StatusCodes._

  def routes: Route = trendRoute ~ startLambdaRoute ~ stopLambdaRoute ~ trendQueriesRoute ~ trendQueryPostRoute ~ trendQueryGetDelRoute ~ startQueryRoute ~ stopQueryRoute ~ getQueryResultRoute

  def trendRoute =
    pathPrefix("events" / Segment / "popular") { event =>
      pathEndOrSingleSlash {
        val name = s"${event}_lambda"
        post {
          // POST /events/:event/popular
          onSuccess(createTrendLambda(name, event, "popular", "retweet")) { 
            case LambdaSupervisor.TrendLambdaCreated(lambda) => complete(Created, lambda) 
            case LambdaSupervisor.TrendLambdaExists =>
                val err = Error(s"Popular lambda exists already.")
                complete(BadRequest, err) 
            case LambdaSupervisor.TrendLambdaNoEventError =>
                val err = Error(s"Event ${event} is not found.")
                complete(BadRequest, err) 
          }
        } ~
        get {
          // GET /events/:event/popular
          onSuccess(getTrendLambda(name)) {
            _.fold(complete(NotFound))(e => complete(OK, e))
          }
        } ~
        delete {
          // DELETE /events/:event/popular
          onSuccess(cancelTrendLambda(name)) {
            _.fold(complete(NotFound))(e => complete(OK, e))
          }
        }
      }
    }

  def startLambdaRoute =
    pathPrefix("events" / Segment / "popular" / "start") { event =>
      pathEndOrSingleSlash {
        post {
          // POST /events/:event/popular/start
          onSuccess(startTrendLambda(s"${event}_lambda")) { 
            _.fold(complete(NotFound))(e => complete(OK, e))
          }
        }
      }
    }

  def stopLambdaRoute =
    pathPrefix("events" / Segment / "popular" / "stop") { event =>
      pathEndOrSingleSlash {
        post {
          // POST /events/:event/popular/stop
          onSuccess(stopTrendLambda(s"${event}_lambda")) { 
            _.fold(complete(NotFound))(e => complete(OK, e))
          }
        }
      }
    }

  // ===================== Query Routes ======================

  def trendQueriesRoute =
    pathPrefix("events" / Segment) { event =>
      pathPrefix("popular" / Segment / "query" / "all") { attribute =>
        pathEndOrSingleSlash {
          get {
             // GET /events/:event/popular/:attribute/query/all
             onSuccess(getTrendQueries(event, attribute)) { trendqueries =>
               complete(OK, trendqueries)
            }
          }
        }
      }
    }

  def trendQueryPostRoute =
    pathPrefix("events" / Segment) { event =>
      pathPrefix("popular" / Segment / "query") { attribute =>
        pathEndOrSingleSlash {
          post {
            // POST /events/:event/popular/:attribute/query
            entity(as[QueryDescription]) { td => 
              onSuccess(createTrendQuery(event, "popular", attribute, td.lowerTime, td.upperTime, td.trendType, td.value)) { 
                case ServingLayerSupervisor.TrendQueryCreated(query) => complete(Created, query) 
                case ServingLayerSupervisor.TrendQueryExists =>
                    val err = Error(s"Similar $attribute query exists already.")
                    complete(BadRequest, err) 
              }
            }
          }
        }
      }
    }

  def trendQueryGetDelRoute =
    pathPrefix("events" / Segment) { event =>
      pathPrefix("popular" / Segment) { attribute =>      
        pathPrefix("query" / Segment) { name =>
          pathEndOrSingleSlash {
            get {
               // GET /events/:event/popular/:attribute/query/:name
              onSuccess(getTrendQuery(name, event, attribute)) {
                _.fold(complete(NotFound))(e => complete(OK, e))
              }
            } ~
            delete {
               // GET /events/:event/popular/:attribute/query/:name
              onSuccess(cancelTrendQuery(name, event, attribute)) {
                _.fold(complete(NotFound))(e => complete(OK, e))
              }
            }
          }
        }
      }
    }

  def startQueryRoute =
    pathPrefix("events" / Segment) { event =>
      pathPrefix("popular" / Segment) { attribute =>      
        pathPrefix("query" / Segment / "start") { name =>
          pathEndOrSingleSlash {
            post {
              // POST /events/:event/popular/:attribute/query/:name/start
              onSuccess(startTrendQuery(name, event, attribute)) { resultrows =>
                complete(OK, resultrows)
              }
            }
          }
        }
      }
    }

  def stopQueryRoute =
    pathPrefix("events" / Segment) { event =>
      pathPrefix("popular" / Segment) { attribute =>      
        pathPrefix("query" / Segment / "stop") { name =>
          pathEndOrSingleSlash {
            post {
              // POST /events/:event/popular/:attribute/query/:name/stop
              onSuccess(stopTrendQuery(name, event, attribute)) { 
                _.fold(complete(NotFound))(e => complete(OK, e))
              }
            }
          }
        }
      }
    }

  def getQueryResultRoute =
    pathPrefix("events" / Segment) { event =>
      pathPrefix("popular" / Segment) { attribute =>      
        pathPrefix("query" / Segment / "result") { name =>
          pathEndOrSingleSlash {
            get {
              // GET /events/:event/popular/:attribute/query/:name/result
              onSuccess(getTrendQueryResult(name, event, attribute)) { results =>
                complete(OK, results)
              }
            }
          }
        }
      }
    }
}


trait LambdaSupervisorApi {
  import LambdaSupervisor._
  import ServingLayerSupervisor._

  def createLambdaSupervisor(): ActorRef
  def selectServingLayer(event: String, atrribute: String): ActorSelection

  implicit def executionContext: ExecutionContext
  implicit def requestTimeout: Timeout

  lazy val lambdaSupervisor = createLambdaSupervisor()
  println(s"\nLambdaSepervisor is created with Actor Reference ${lambdaSupervisor}.\n")
  
  // lambdaSupervisor.ask(InitializeSchema)
  // println("\n-----------------------------------------\n")
  // initialize
  // println("SCHEMA CREATED.")
  // println("\n\n-----------------------------------------\n")


  def getTrendLambdas(event: String) =
    lambdaSupervisor.ask(GetTrendLambdas(event))
    .mapTo[TrendLambdas]

  def createTrendLambda(name: String, event: String, query_type: String, attribute: String,
    speed_job_running: Boolean=false, batch_job_running: Boolean=false,
    speed_job_id: String="", batch_job_id: String="") = {
    lambdaSupervisor.ask(CreateTrendLambda(name, event, query_type, attribute, speed_job_running, batch_job_running, speed_job_id, batch_job_id, false))
      .mapTo[TrendLambdaResponse]
  }

  def getTrendLambda(name: String) =
    lambdaSupervisor.ask(GetTrendLambda(name))
      .mapTo[Option[TrendLambda]]

  def cancelTrendLambda(name: String) =
    lambdaSupervisor.ask(CancelTrendLambda(name))
      .mapTo[Option[TrendLambda]]

 def startTrendLambda(name: String) =
    lambdaSupervisor.ask(StartTrendLambda(name))
      .mapTo[Option[TrendLambda]]

  def stopTrendLambda(name: String) =
    lambdaSupervisor.ask(StopTrendLambda(name))
      .mapTo[Option[TrendLambda]]

 def createTrendQuery(event: String, query_type: String, attribute: String, lowerTime: String, upperTime: String, 
    trendType: String, value: String, running: Boolean=false, job_id: String="") = {
    val servingLayer = selectServingLayer(event, attribute)
    //Create a name for the new query
    val responseFuture = servingLayer ? GetTrendQueries
    val result = Await.result(responseFuture, 10 seconds).asInstanceOf[TrendQueries]
    val query_num = result.query_num
    val id = "%02d".format(query_num+1).toString
    val name = s"${event}_${query_type}_${attribute}_${id}"
    println(s"\n${event}: Calling ServingLayerActor.CreateTrendQuery by ${servingLayer} for creating new query ${name}")
    servingLayer.ask(CreateTrendQuery(name, lowerTime, upperTime, trendType, value, running, job_id, false))
      .mapTo[TrendQueryResponse]
  }

  def getTrendQueries(event: String, attribute: String) = {
    val servingLayer = selectServingLayer(event, attribute)
    println(s"\n${event}: Calling ServingLayerActor.GetTrendQueries by ${servingLayer}")
    servingLayer.ask(GetTrendQueries)
    .mapTo[TrendQueries]
  }

  def getTrendQuery(name: String, event: String, attribute: String) = {
    val servingLayer = selectServingLayer(event, attribute)
    println(s"\n${event}: Calling ServingLayerActor.GetTrendQuery by ${servingLayer}")
    servingLayer.ask(GetTrendQuery(name))
      .mapTo[Option[TrendQuery]]
  }

  def cancelTrendQuery(name: String, event: String, attribute: String) = {
    val servingLayer = selectServingLayer(event, attribute)
    println(s"\n${event}: Calling ServingLayerActor.CancelTrendQuery by ${servingLayer}")
    servingLayer.ask(CancelTrendQuery(name))
      .mapTo[Option[TrendQuery]]
  }

  def startTrendQuery(name: String, event: String, attribute: String) = {
    val servingLayer = selectServingLayer(event, attribute)
    println(s"\n${event}: Calling ServingLayerActor.StartTrendQuery by ${servingLayer}")
     servingLayer.ask(StartTrendQuery(name))
      .mapTo[Option[TrendQuery]]
  }

  def stopTrendQuery(name: String, event: String, attribute: String) = {
    val servingLayer = selectServingLayer(event, attribute)
    println(s"\n${event}: Calling ServingLayerActor.StopTrendQuery by ${servingLayer}")
    servingLayer.ask(StopTrendQuery(name))
      .mapTo[Option[TrendQuery]]
  }

  def getTrendQueryResult(name: String, event: String, attribute: String) = {
    val servingLayer = selectServingLayer(event, attribute)
    println(s"\n${event}: Calling ServingLayerActor.GetTrendQueryResult by ${servingLayer}")
    servingLayer.ask(GetTrendQueryResult(name))
      .mapTo[Vector[ServingLayerSupervisor.PopularQueryResult]]

  }

}

