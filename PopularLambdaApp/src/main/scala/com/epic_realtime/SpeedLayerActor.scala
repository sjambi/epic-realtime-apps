package com.epic_realtime

import scala.concurrent.duration._
import scala.concurrent.Future
import akka.actor._
import akka.util.Timeout
import akka.pattern.ask
import com.typesafe.config.{ Config, ConfigFactory } 
import com.epic_realtime.utils.SparkJob
import java.io._
import com.epic_realtime.utils.CassandraClient

object SpeedLayerActor {
  def props(name: String) = Props(new SpeedLayerActor(name))

  case class Run(event: String)
  case class Stop(event: String)
  case object Cancel

}

class SpeedLayerActor(name: String) extends Actor {
  import SpeedLayerActor._

  override def preStart() {
    // println(s"........ SpeedLayerActor (${name}) is started with path ${context.self.path}.")
    println(s"........ SpeedLayerActor is created as ${context.self}.")
  }

  val config = ConfigFactory.load() 
  val app_root = config.getString("app.root")
  val app_name = config.getString("app.name")


  def receive = {

    case Run(event) =>
      println(s"${event}: Speed Layer is running for retweet lambda as actor ${name}")
      val mainclass = "com.epic_realtime.SpeedLayer"
      val jarfile = "retweet/target/scala-2.10/epic_realtime-assembly-1.0.jar"
      if (new File(app_root+jarfile).exists){
        println(s"Found Spark Spead Job file ${app_root+jarfile}")
        val speed_job_id = SparkJob.runLambda(app_root+jarfile, app_name, mainclass, event, "speed")
        val cassandraClient = new CassandraClient()
        cassandraClient.setLambdaSpeedJobId(event, "popular", "retweet", speed_job_id)
        cassandraClient.setLambdaSpeedStatus(event, "popular", "retweet", true)
        cassandraClient.close       
      } else {
       println(s"Error =====> Spark Spark Spead Job file ${app_root+jarfile} does not exist.")
      }


    case Stop(event) =>
      println(s"${event}: Speed Layer has stopped as actor ${name}")
      val cassandraClient = new CassandraClient()
      val speed_job_id = cassandraClient.getLambdaSpeedJobId(event, "popular", "retweet")
      if (speed_job_id.nonEmpty) SparkJob.killJob(event, speed_job_id)
      cassandraClient.setLambdaSpeedJobId(event, "popular", "retweet", "")
      cassandraClient.setLambdaSpeedStatus(event, "popular", "retweet", false)
      cassandraClient.close


    case Cancel =>
      self ! PoisonPill
 

  }
}

