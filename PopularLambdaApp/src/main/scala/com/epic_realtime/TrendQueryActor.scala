package com.epic_realtime
import akka.actor.{ Actor, Props, PoisonPill }

import scala.concurrent.duration._
import scala.concurrent.Future
import akka.util.Timeout
import akka.pattern.pipe
import com.epic_realtime.utils.SparkJob
import com.epic_realtime.utils.CassandraClient
import com.datastax.driver.core.Row
import com.typesafe.config.{ Config, ConfigFactory } 
import com.epic_realtime.utils.CassandraUtils.resultset._
import scala.collection.JavaConversions._
import java.io._

import ServingLayerSupervisor.PopularQueryResult

object TrendQueryActor {
  def props(name: String) = Props(new TrendQueryActor(name))
 
  case class Event(name: String = "")
  case class QueryType(q_type: String = "")  
  case class Attribute(attr: String = "")
  case class LowerTime(time: String = "")
  case class UpperTime(time: String = "")
  case class TrendType(t: String = "")
  case class Value(v: String = "")
  case class Running(s: Boolean)
  case class SetJobId(job: String)
  case object SaveTrendQuery
  case object GetTrendQuery 
  case object CancelQuery 
  case object StartQuery
  case object StopQuery
  case object GetResult
  case object RunQuery 
}


class TrendQueryActor(name: String) extends Actor {
  import TrendQueryActor._
  import context._

  var event = ""
  var query_type = ""  
  var attribute = ""
  var lowerTime = ""
  var upperTime = ""
  var trendType = ""
  var value = ""
  var running = false
  var job_id = ""

  def buildTweetRow(r: Row): PopularQueryResult = {
    val value = r.getString("retweet_id")
    val screen_name = r.getString("retweet_screen_name")
    val text = r.getString("retweet_text")
    val count = r.getInt("count")
    PopularQueryResult(value, screen_name, text, count)
  }

  def buildUserRow(r: Row): PopularQueryResult = {
    val value = r.getString("retweet_user_id")
    val screen_name = r.getString("retweet_screen_name")
    val text = r.getString("retweet_text")    
    val count = r.getInt("count")
    PopularQueryResult(value, screen_name, text, count)
  }

  val config = ConfigFactory.load() 
  val app_root = config.getString("app.root")
  val app_name = config.getString("app.name")


  def receive = {
   
    case Event(e) =>  event = e

    case QueryType(q_type) => query_type = q_type

    case Attribute(attr) =>  attribute = attr

    case LowerTime(ltime) =>  lowerTime = ltime

    case UpperTime(utime) =>  upperTime = utime

    case TrendType(t) =>  trendType = t

    case Value(v) => value = v

    case Running(s) =>  running = s

    case SetJobId(job) => job_id = job


    case SaveTrendQuery =>
      println(s"${event}: Popular query $name is created.")
      val cassandraClient = new CassandraClient()
      cassandraClient.addQuery(name, event, query_type, attribute, 
        lowerTime, upperTime, trendType, value, running, job_id)
      cassandraClient.close


    case GetTrendQuery => 
      val cassandraClient = new CassandraClient()
      job_id = cassandraClient.getQueryJobId(name)
      cassandraClient.close 
      sender() ! Some(ServingLayerSupervisor.TrendQuery(name, event, query_type, attribute, 
        lowerTime, upperTime, trendType, value, running, job_id)) 


    case CancelQuery => 
      println(s"${event}: Popular query $name is deleted.")
      // self ! StopQuery //a problem of getting the spark job id after the recored has been deleted
      val cassandraClient = new CassandraClient()
      cassandraClient.deleteQuery(name)
      cassandraClient.close
      sender() ! Some(ServingLayerSupervisor.TrendQuery(name, event, query_type, attribute, 
        lowerTime, upperTime, trendType, value, running, job_id))
      self ! PoisonPill


    case StartQuery =>
      println(s"${event}: Popular query $name is started.")
      running = true
      println(s"${event}: $name's running status is $running.")
      self ! RunQuery
      // //Retrieve Spark job ids
      //delay?
      val cassandraClient = new CassandraClient()
      job_id = cassandraClient.getQueryJobId(name)
      cassandraClient.setQueryStatus(name, true)      
      cassandraClient.close
      sender() ! Some(ServingLayerSupervisor.TrendQuery(name, event, query_type, attribute, 
        lowerTime, upperTime, trendType, value, running, job_id))


    case StopQuery =>
      println(s"${event}: Popular query $name is stopped.")
      val cassandraClient = new CassandraClient()
      job_id = cassandraClient.getQueryJobId(name)
      cassandraClient.setQueryStatus(name, false)
      cassandraClient.setQueryJobId(name, "")
      cassandraClient.close
      if (job_id.nonEmpty) SparkJob.killJob(event, job_id)     
      running = false
      job_id = ""
      println(s"${event}: $name's running status is $running.")
      sender() ! Some(ServingLayerSupervisor.TrendQuery(name, event, query_type, attribute, 
        lowerTime, upperTime, trendType, value, running, job_id))


    case GetResult =>
      val cassandraClient = new CassandraClient()     
      val resultFuture = cassandraClient.getAsyncQueryResult(name)
      cassandraClient.close
      attribute match{
          case "tweet" =>
            resultFuture map(_.all().map(buildTweetRow).toVector.sortBy(-_.count)) pipeTo sender 
          case "user" =>
            resultFuture map(_.all().map(buildUserRow).toVector.sortBy(-_.count)) pipeTo sender
      } 

    case RunQuery =>
      println(s"${event}: Serving Layer is running popular ${attribute} query $name.")
      val mainclass = s"com.epic_realtime.${attribute.capitalize}ServingLayer"
      val jarfile = "retweet/target/scala-2.10/epic_realtime-assembly-1.0.jar"
      if (new File(app_root+jarfile).exists){
        println(s"Found Spark ${attribute.capitalize} Serving Job file ${app_root+jarfile}")       
        val spark_job_driverid = SparkJob.runQuery(app_root+jarfile, app_name, mainclass, 
          event, name, trendType, value)
        val cassandraClient = new CassandraClient()
        cassandraClient.setQueryJobId(name, spark_job_driverid)
        cassandraClient.close       
      } else {
       println(s"Error =====> Spark ${attribute.capitalize} Serving Job file ${app_root+jarfile} does not exist.")
      }
   
  }
}



