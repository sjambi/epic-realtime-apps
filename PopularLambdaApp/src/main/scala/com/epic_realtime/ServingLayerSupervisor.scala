package com.epic_realtime

import scala.concurrent.duration._
import scala.concurrent.Future
import akka.actor._
import akka.util.Timeout
import com.typesafe.config.{ Config, ConfigFactory } 
import com.epic_realtime.utils.CassandraClient
import scala.collection.JavaConversions._
import com.epic_realtime.utils.SparkJob
import java.io._

object MyImplicits {
  implicit val timeout = Timeout(30 seconds)
}
import MyImplicits._;

object ServingLayerSupervisor {
  // def props(implicit timeout: Timeout) = Props(new ServingLayerSupervisor)
  // def name = "servingLayerSupervisor"
  def props(name: String) = Props(new ServingLayerSupervisor(name))

  case class Event(event: String = "")
  case class QueryType(q_type: String = "")  
  case class Attribute(attr: String = "")
  case class CreateTrendQuery(name: String, lowerTime: String, upperTime: String, 
    trendType: String, value: String, running: Boolean, job_id: String, loaded: Boolean) 
  case class InitializeSchema(name: String)
  case class GetTrendQuery(name: String) 
  case object GetTrendQueries
  case class CancelTrendQuery(name: String) 
  case class StartTrendQuery(name: String) 
  case class StopTrendQuery(name: String)
  case class GetTrendQueryResult(name: String)
  case object Cancel
  // case class JobDrivers(query_name: String, job1: String, job2: String, job3: String)


  case class TrendQuery(name: String, event: String, query_type: String, attribute: String, 
    lowerTime: String ="", upperTime: String ="", trendType: String ="", value: String ="",
    running: Boolean = false, job_id: String ="")
  case class TrendQueries(queries: Vector[TrendQuery] = Vector.empty[TrendQuery]) {
    def query_num: Int = { return queries.size }
  }

  sealed trait TrendQueryResponse 
  case class TrendQueryCreated(trendQuery: TrendQuery) extends TrendQueryResponse 
  case object TrendQueryError extends TrendQueryResponse 
  case object TrendQueryExists extends TrendQueryResponse 

  case class PopularQueryResult(value: String, screen_name: String, text:String, count: Int)

}

class ServingLayerSupervisor(name: String) extends Actor {
// class ServingLayerSupervisor(implicit timeout: Timeout) extends Actor {

  import ServingLayerSupervisor._
  import context._

  var event = ""
  var query_type = ""
  var attribute = ""

  def createTrendQueryActor(name: String) =
    context.actorOf(TrendQueryActor.props(name), name) 

  def intialize_loadQueries() = {
    val cassandraClient = new CassandraClient()
    cassandraClient.createSchema
    val querySet = cassandraClient.getQueries(query_type,attribute)
    for (q <- querySet) {
      var query_name = q.getString("query_name")
      var event_name = q.getString("event_name") 
      var query_type = q.getString("query_type")       
      var attribute = q.getString("attribute")
      var lower_time = q.getString("lower_time")
      var upper_time = q.getString("upper_time")
      var trend_type = q.getString("trend_type")
      var trend_value = q.getString("trend_value")
      var running = q.getBool("running")
      var job_id = q.getString("job_id")
      self ! CreateTrendQuery(query_name, lower_time, upper_time, 
        trend_type, trend_value, running, job_id, true) 
    }
    cassandraClient.close
  }

  override def preStart() {
    //intialize_loadQueries
    // println(s"........ ServingLayerActor (${name}) is started with Actor Reference ${context.self.path}.")
    println(s"........ ServingLayerActor is created as ${context.self}.")
    //println(s"I'm serving $attribute ####################")
  }

 
  def receive = {

    case Event(e) =>  
      event = e
      println(s"........ $name is serving $event event.")

   case QueryType(q_type) => 
      query_type = q_type

    case Attribute(attr) =>  
	    attribute = attr
      intialize_loadQueries
      println(s"........ $name is serving $attribute trend.")

    case InitializeSchema(name) =>     
      println(s"${event}: Query schema is created for $name.")
      println("\n------------------------------------------------\n")
      val cassandraClient = new CassandraClient()
      attribute match {
        case "tweet" =>
          cassandraClient.createPopularQuerySchema_tweet(name)
        case "user" =>
          cassandraClient.createPopularQuerySchema_user(name)
      }
      cassandraClient.close

    case CreateTrendQuery(name, lowerTime, upperTime, trendType, value, running, job_id, loaded) =>
      def create() = {  
        val trendQueryActor = createTrendQueryActor(name)
        trendQueryActor ! TrendQueryActor.Event(event)
        trendQueryActor ! TrendQueryActor.QueryType(query_type)
        trendQueryActor ! TrendQueryActor.Attribute(attribute)
        trendQueryActor ! TrendQueryActor.LowerTime(lowerTime)
        trendQueryActor ! TrendQueryActor.UpperTime(upperTime)
        trendQueryActor ! TrendQueryActor.TrendType(trendType)
        trendQueryActor ! TrendQueryActor.Value(value) 
        trendQueryActor ! TrendQueryActor.Running(running) 
        trendQueryActor ! TrendQueryActor.SetJobId(job_id)
        if (!loaded){
          self ! InitializeSchema(name)
          trendQueryActor ! TrendQueryActor.SaveTrendQuery 
        }
        sender() ! TrendQueryCreated(TrendQuery(name, event, query_type, attribute, 
          lowerTime, upperTime, trendType, value, running, job_id))
        println(s"${event}: Creating TrendQueryActor as (${name}) with Actor Reference ${trendQueryActor}")
        println(s"${event}: TrendQueryActor's attribute is (${attribute})")
      }
      context.child(name).fold(create())(_ => sender() ! TrendQueryExists) 


    case GetTrendQuery(name) =>
      def notFound() = sender() ! None
      def getTrendQuery(child: ActorRef) = child forward TrendQueryActor.GetTrendQuery
      context.child(name).fold(notFound())(getTrendQuery)

    
    case GetTrendQueries =>
      import akka.pattern.ask
      import akka.pattern.pipe
      def getTrendQueries = context.children.map { child =>
        self.ask(GetTrendQuery(child.path.name)).mapTo[Option[TrendQuery]]
      }
      def convertToTrendQueries(f: Future[Iterable[Option[TrendQuery]]]) =
        f.map(_.flatten).map(l=> TrendQueries(l.toVector)) 
      pipe(convertToTrendQueries(Future.sequence(getTrendQueries))) to sender()
    

    case CancelTrendQuery(name) =>
      def notFound() = sender() ! None
      def cancelTrendQuery(child: ActorRef) = child forward TrendQueryActor.CancelQuery
      context.child(name).fold(notFound())(cancelTrendQuery)


    case StartTrendQuery(name) =>
      def notFound() = sender() ! None
      def startTrendQuery(child: ActorRef) = child forward TrendQueryActor.StartQuery
      context.child(name).fold(notFound())(startTrendQuery)
   

    case StopTrendQuery(name) =>
      def notFound() = sender() ! None
      def stopTrendQuery(child: ActorRef) = child forward TrendQueryActor.StopQuery
      context.child(name).fold(notFound())(stopTrendQuery)


    case GetTrendQueryResult(name) =>
      def notFound() = sender() ! None
      def getTrendQueryResult(child: ActorRef) = child forward TrendQueryActor.GetResult
      context.child(name).fold(notFound())(getTrendQueryResult)


    case Cancel =>
      //Cancell all query children
      import akka.pattern.ask
      import akka.pattern.pipe
      def cancelTrendQueries = context.children.map { child =>
        self.ask(CancelTrendQuery(child.path.name)).mapTo[Option[TrendQuery]]
      }
      def convertToTrendQueries(f: Future[Iterable[Option[TrendQuery]]]) =
        f.map(_.flatten).map(l=> TrendQueries(l.toVector)) 
      pipe(convertToTrendQueries(Future.sequence(cancelTrendQueries))) to sender()
      self ! PoisonPill      


  }
  
}

