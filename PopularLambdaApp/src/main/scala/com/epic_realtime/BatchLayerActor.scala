package com.epic_realtime

import scala.concurrent.duration._
import scala.concurrent.Future
import akka.actor._
import akka.util.Timeout
import akka.pattern.ask
import com.typesafe.config.{ Config, ConfigFactory } 
import com.epic_realtime.utils.SparkJob
import java.io._
import com.epic_realtime.utils.CassandraClient

object BatchLayerActor {
  def props(name: String) = Props(new BatchLayerActor(name))

  case class Run(event: String)
  case class Stop(event: String)
  case object Cancel

}

class BatchLayerActor(name: String) extends Actor {
  import BatchLayerActor._

  override def preStart() {
    // println(s"........ BatchLayerActor (${name}) is started with path ${context.self.path}.")
    println(s"........ BatchLayerActor is created as ${context.self}.")
  }

  val config = ConfigFactory.load() 
  val app_root = config.getString("app.root")
  val app_name = config.getString("app.name")


  def receive = {

    case Run(event) =>
      println(s"${event}: Batch Layer is running for retweet lambda as actor ${name}")
      val mainclass = "com.epic_realtime.BatchLayer"   
      val jarfile = "retweet/target/scala-2.10/epic_realtime-assembly-1.0.jar"
      if (new File(app_root+jarfile).exists){
        println(s"Found Spark Batch Job file ${app_root+jarfile}")
        val batch_job_id = SparkJob.runLambda(app_root+jarfile, app_name, mainclass, event, "batch")
        val cassandraClient = new CassandraClient()
        cassandraClient.setLambdaBatchJobId(event, "popular", "retweet", batch_job_id)
        cassandraClient.setLambdaBatchStatus(event, "popular", "retweet", true)
        cassandraClient.close
      } else {
       println(s"Error =====> Spark Spark Batch Job file ${app_root+jarfile} does not exist.")
      }


    case Stop(event) =>
      println(s"${event}: Batch Layer has stopped as actor ${name}")
      val cassandraClient = new CassandraClient()
      val batch_job_id = cassandraClient.getLambdaBatchJobId(event, "popular", "retweet")
      if (batch_job_id.nonEmpty) SparkJob.killJob(event, batch_job_id)
      cassandraClient.setLambdaBatchJobId(event, "popular", "retweet", "")
      cassandraClient.setLambdaBatchStatus(event, "popular", "retweet", false)
      cassandraClient.close
    

    case Cancel =>
      self ! PoisonPill

  }
}
