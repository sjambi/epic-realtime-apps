package com.epic_realtime
import akka.actor.{ Actor, Props, PoisonPill }

import scala.concurrent.duration._
import scala.concurrent.Future
import akka.util.Timeout
import akka.pattern.pipe
import com.epic_realtime.utils.SparkJob
import com.epic_realtime.utils.CassandraClient
import com.datastax.driver.core.Row
import com.typesafe.config.{ Config, ConfigFactory } 
import com.epic_realtime.utils.CassandraUtils.resultset._
import scala.collection.JavaConversions._
import java.io._

import DatasetServingLayerSupervisor.DatasetQueryResult

object DatasetQueryActor {
  def props(name: String) = Props(new DatasetQueryActor(name))
 
  case class Event(name: String = "")
  case class QueryType(q_type: String = "")  
  case class Attribute(attr: String = "")
  case class LowerTime(time: String = "")
  case class UpperTime(time: String = "")
  case class TrendType(t: String = "")
  case class Value(v: String = "")
  case class Running(s: Boolean)
  case class SetJobId(job: String)
  case object SaveDatasetQuery
  case object GetDatasetQuery 
  case object CancelQuery 
  case object StartQuery
  case object StopQuery
  case object GetResult
  case object RunQuery 
}


class DatasetQueryActor(name: String) extends Actor {
  import DatasetQueryActor._
  import context._

  var event = ""
  var query_type = ""  
  var attribute = ""
  var lowerTime = ""
  var upperTime = ""
  var trendType = ""
  var value = ""
  var running = false
  var job_id = ""


  def buildHashtagRow(r: Row): DatasetQueryResult = {
    val value = r.getString("hashtag")
    DatasetQueryResult(value)
  }

  def buildUserRow(r: Row): DatasetQueryResult = {
    val value = r.getString("user")
    DatasetQueryResult(value)
  }

  def buildMentionRow(r: Row): DatasetQueryResult = {
    val value = r.getString("mention")
    DatasetQueryResult(value)
  }

  def buildUrlRow(r: Row): DatasetQueryResult = {
    val value = r.getString("url")
    DatasetQueryResult(value)
  }

  val config = ConfigFactory.load() 
  val app_root = config.getString("app.root")
  val app_name = config.getString("app.name")


  def receive = {
   
    case Event(e) =>  event = e

    case QueryType(q_type) => query_type = q_type

    case Attribute(attr) =>  attribute = attr

    case LowerTime(ltime) =>  lowerTime = ltime

    case UpperTime(utime) =>  upperTime = utime

    case TrendType(t) =>  trendType = t

    case Value(v) => value = v

    case Running(s) =>  running = s

    case SetJobId(job) => job_id = job


    case SaveDatasetQuery =>
      println(s"${event}: Dataset query $name is created.")
      val cassandraClient = new CassandraClient()
      cassandraClient.addQuery(name, event, query_type, attribute, 
        lowerTime, upperTime, trendType, value, running, job_id)
      cassandraClient.close


    case GetDatasetQuery => 
      println(s"${event}: Dataset query $name is retrieved for $attribute.")
      val cassandraClient = new CassandraClient()
      job_id = cassandraClient.getQueryJobId(name)
      cassandraClient.close 
      sender() ! Some(DatasetServingLayerSupervisor.DatasetQuery(name, event, query_type, attribute, 
        lowerTime, upperTime, trendType, value, running, job_id)) 


    case CancelQuery => 
      println(s"${event}: Dataset query $name is deleted.")
      // self ! StopQuery //a problem of getting the spark job id after the recored has been deleted
      val cassandraClient = new CassandraClient()
      cassandraClient.deleteQuery(name)
      cassandraClient.close
      sender() ! Some(DatasetServingLayerSupervisor.DatasetQuery(name, event, query_type, attribute, 
        lowerTime, upperTime, trendType, value, running, job_id))
      self ! PoisonPill


    case StartQuery =>
      println(s"${event}: Dataset query $name is started.")
      running = true
      println(s"${event}: $name's running status is $running.")
      self ! RunQuery
      // //Retrieve Spark job ids
      //delay?
      val cassandraClient = new CassandraClient()
      job_id = cassandraClient.getQueryJobId(name)
      cassandraClient.setQueryStatus(name, true)      
      cassandraClient.close
      sender() ! Some(DatasetServingLayerSupervisor.DatasetQuery(name, event, query_type, attribute, 
        lowerTime, upperTime, trendType, value, running, job_id))


    case StopQuery =>
      println(s"${event}: Popular query $name is stopped.")
      val cassandraClient = new CassandraClient()
      job_id = cassandraClient.getQueryJobId(name)
      cassandraClient.setQueryStatus(name, false)
      cassandraClient.setQueryJobId(name, "")
      cassandraClient.close
      if (job_id.nonEmpty) SparkJob.killJob(event, job_id)     
      running = false
      job_id = ""
      println(s"${event}: $name's running status is $running.")
      sender() ! Some(DatasetServingLayerSupervisor.DatasetQuery(name, event, query_type, attribute, 
        lowerTime, upperTime, trendType, value, running, job_id))


    case GetResult =>
      val cassandraClient = new CassandraClient()     
      val resultFuture = cassandraClient.getAsyncQueryResult(name)
      cassandraClient.close
      attribute match{
        case "hashtag" =>
            resultFuture map(_.all().map(buildHashtagRow).toVector.sortBy(_.value)) pipeTo sender        
        case "user" =>
            println("FOUND")
            resultFuture map(_.all().map(buildUserRow).toVector.sortBy(_.value)) pipeTo sender

        case "mention" =>
            resultFuture map(_.all().map(buildMentionRow).toVector.sortBy(_.value)) pipeTo sender 
        case "url" =>
            resultFuture map(_.all().map(buildUrlRow).toVector.sortBy(_.value)) pipeTo sender 
      } 


    case RunQuery =>
      println(s"${event}: Dataset query $name is calling RunQuery for $attribute.")
      println(s"${event}: Serving Layer is running dataset ${attribute} query $name.")
      val mainclass = s"com.epic_realtime.DatasetServingLayer"
      val jarfile = s"$attribute/target/scala-2.10/epic_realtime-assembly-1.0.jar"
      if (new File(app_root+jarfile).exists){
        println(s"Found Spark Dataset ${attribute.capitalize} Serving Job file ${app_root+jarfile}")       
        val spark_job_driverid = SparkJob.runDatasetQuery(app_root+jarfile, app_name, mainclass, 
          event, name, lowerTime, upperTime)
        val cassandraClient = new CassandraClient()
        cassandraClient.setQueryJobId(name, spark_job_driverid)
        cassandraClient.close       
      } else {
       println(s"Error =====> Spark Dataset ${attribute.capitalize} Serving Job file ${app_root+jarfile} does not exist.")
      }
   
  }
}



