package com.epic_realtime

import akka.actor.{ Actor, Props, PoisonPill }

import scala.concurrent.duration._
import scala.concurrent.Future

import akka.actor._
import akka.util.Timeout
import akka.pattern.ask

import com.epic_realtime.utils.CassandraClient


object TrendLambdaActor {
  def props(name: String) = Props(new TrendLambdaActor(name))
 
  case class Event(event: String = "")
  case class QueryType(q_type: String = "")  
  case class Attribute(attr: String = "")
  case class SpeedJobRunning(status: Boolean)
  case class BatchJobRunning(status: Boolean)
  case class SetSpeedJobIds(s_job: String) 
  case class SetBatchJobIds(b_job: String)
  case object SaveTrendLambda
  case object GetTrendLambda 
  case object CancelTrendLambda 
  case object Start
  case object Stop

}

class TrendLambdaActor(name: String) extends Actor {
  import TrendLambdaActor._
  import context._

  var event = ""
  var query_type = ""
  var attribute = ""
  var speed_job_running = false
  var batch_job_running = false
  var speed_job_id = ""
  var batch_job_id = ""

  def createSpeedLayer(name: String) = 
    context.actorOf(SpeedLayerActor.props(name), name) 

  def createBatchLayer(name: String) = 
    context.actorOf(BatchLayerActor.props(name), name) 

  def createServingLayerSupervisor(name: String) = 
    context.actorOf(ServingLayerSupervisor.props(name), name) 

 def createDatasetServingLayerSupervisor(name: String) = 
    context.actorOf(DatasetServingLayerSupervisor.props(name), name) 

  val speedLayer = createSpeedLayer(s"${name}_speed")
  val batchLayer = createBatchLayer(s"${name}_batch")
  val servingLayer = createServingLayerSupervisor(s"${name}_serving")
  val datasetServingLayer = createDatasetServingLayerSupervisor(s"${name}_serving_dataset")

  
  def receive = {

    case Event(e) =>  
    	event = e
    	servingLayer ! ServingLayerSupervisor.Event(event)
      datasetServingLayer ! DatasetServingLayerSupervisor.Event(event)

    case QueryType(q_type) => 
      query_type = q_type
      servingLayer ! ServingLayerSupervisor.QueryType(query_type)
      datasetServingLayer ! DatasetServingLayerSupervisor.QueryType("dataset")

    case Attribute(attr) =>  
    	attribute = attr
    	servingLayer ! ServingLayerSupervisor.Attribute(attribute)
      datasetServingLayer ! DatasetServingLayerSupervisor.Attribute(attribute)

    case SpeedJobRunning(s) =>  speed_job_running = s

    case BatchJobRunning(s) =>  batch_job_running = s

    case SetSpeedJobIds(s_job) => speed_job_id = s_job

    case SetBatchJobIds(b_job) => batch_job_id = b_job      


    case SaveTrendLambda =>
      println(s"${event}: Trend lambda $name is created.")
      val cassandraClient = new CassandraClient()
      cassandraClient.addLambda(event, query_type, attribute, speed_job_running, batch_job_running, speed_job_id, batch_job_id)
      cassandraClient.close


    case GetTrendLambda => 
      val cassandraClient = new CassandraClient()
      speed_job_id = cassandraClient.getLambdaSpeedJobId(event, query_type, attribute)
      batch_job_id = cassandraClient.getLambdaBatchJobId(event, query_type, attribute)
      cassandraClient.close       
      sender() ! Some(LambdaSupervisor.TrendLambda(event, query_type, attribute, speed_job_running, batch_job_running, speed_job_id, batch_job_id))


    case CancelTrendLambda =>
      println(s"${event}: Trend lambda $name is deleted.")
      // self ! Stop //a problem of getting the spark job id after the recored has been deleted
      speedLayer ! SpeedLayerActor.Cancel
      batchLayer ! BatchLayerActor.Cancel
      servingLayer ! ServingLayerSupervisor.Cancel
      datasetServingLayer ! ServingLayerSupervisor.Cancel
      val cassandraClient = new CassandraClient()
      cassandraClient.deleteLambda(event, query_type, attribute)
      cassandraClient.close
      sender() ! Some(LambdaSupervisor.TrendLambda(event, query_type, attribute, speed_job_running, batch_job_running, speed_job_id, batch_job_id))
      self ! PoisonPill
 

    case Start =>
      println(s"${event}: Trend lambda $name is started.")
      speed_job_running = true
      speedLayer ! SpeedLayerActor.Run(event, attribute)
      batch_job_running = true
      batchLayer ! BatchLayerActor.Run(event, attribute)
      val cassandraClient = new CassandraClient()
      speed_job_id = cassandraClient.getLambdaSpeedJobId(event, query_type, attribute)
      batch_job_id = cassandraClient.getLambdaBatchJobId(event, query_type, attribute)
      cassandraClient.close      
      sender() ! Some(LambdaSupervisor.TrendLambda(event, query_type, attribute, speed_job_running, batch_job_running, speed_job_id, batch_job_id))


    case Stop =>
      println(s"${event}: Trend lambda $name is stopped.")
      speed_job_running = false
      batch_job_running = false
      speed_job_id = ""
      batch_job_id = ""
      speedLayer ! SpeedLayerActor.Stop(event, attribute)
      batchLayer ! BatchLayerActor.Stop(event, attribute)
      val cassandraClient = new CassandraClient()
      sender() ! Some(LambdaSupervisor.TrendLambda(event, query_type, attribute, speed_job_running, batch_job_running, speed_job_id, batch_job_id))

  }
}

