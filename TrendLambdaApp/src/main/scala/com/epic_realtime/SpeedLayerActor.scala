package com.epic_realtime

import scala.concurrent.duration._
import scala.concurrent.Future
import akka.actor._
import akka.util.Timeout
import akka.pattern.ask
import com.typesafe.config.{ Config, ConfigFactory } 
import com.epic_realtime.utils.SparkJob
import java.io._
import com.epic_realtime.utils.CassandraClient

object SpeedLayerActor {
  def props(name: String) = Props(new SpeedLayerActor(name))

  case class Run(event: String, attribute: String)
  case class Stop(event: String, attribute: String)
  case object Cancel

}

class SpeedLayerActor(name: String) extends Actor {
  import SpeedLayerActor._

  override def preStart() {
    // println(s"........ SpeedLayerActor (${name}) is started with path ${context.self.path}.")
    println(s"........ SpeedLayerActor is created as ${context.self}.")
  }

  val config = ConfigFactory.load() 
  val app_root = config.getString("app.root")
  val app_name = config.getString("app.name")


  def receive = {

    case Run(event, attribute) =>
      println(s"${event}: Speed Layer is running for ${attribute} as actor ${name}")
      val mainclass = "com.epic_realtime.SpeedLayer"
      val jarfile = s"${attribute}/target/scala-2.10/epic_realtime-assembly-1.0.jar"
      if (new File(app_root+jarfile).exists){
        println(s"Found Spark Spead Job file ${app_root+jarfile}")
        val speed_job_id = SparkJob.runLambda(app_root+jarfile, app_name, mainclass, event, attribute, "speed")
        val cassandraClient = new CassandraClient()
        cassandraClient.setLambdaSpeedJobId(event, "trend", attribute, speed_job_id)
        cassandraClient.setLambdaSpeedStatus(event, "trend", attribute, true)
        cassandraClient.close       
      } else {
       println(s"Error =====> Spark Spark Spead Job file ${app_root+jarfile} does not exist.")
      }


    case Stop(event, attribute) =>
      println(s"${event}: Speed Layer has stopped as actor ${name}")
      val cassandraClient = new CassandraClient()
      val speed_job_id = cassandraClient.getLambdaSpeedJobId(event, "trend", attribute)
      if (speed_job_id.nonEmpty) SparkJob.killJob(event, speed_job_id)
      cassandraClient.setLambdaSpeedJobId(event, "trend", attribute, "")
      cassandraClient.setLambdaSpeedStatus(event, "trend", attribute, false)
      cassandraClient.close


    case Cancel =>
      self ! PoisonPill

  }
}
