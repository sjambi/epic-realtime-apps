package com.epic_realtime

import scala.concurrent.duration._
import scala.concurrent.Future
import akka.actor._
import akka.util.Timeout
import com.typesafe.config.{ Config, ConfigFactory } 
import com.epic_realtime.utils.CassandraClient
import scala.collection.JavaConversions._
import com.epic_realtime.utils.SparkJob
import java.io._

object MyImplicits2 {
  implicit val timeout = Timeout(30 seconds)
}
import MyImplicits2._;

object DatasetServingLayerSupervisor {
  // def props(implicit timeout: Timeout) = Props(new ServingLayerSupervisor)
  // def name = "servingLayerSupervisor"
  def props(name: String) = Props(new DatasetServingLayerSupervisor(name))

  case class Event(event: String = "")
  case class QueryType(q_type: String = "")  
  case class Attribute(attr: String = "")
  case class CreateDatasetQuery(name: String, lowerTime: String, upperTime: String, 
    trendType: String, value: String, running: Boolean, job_id: String, loaded: Boolean) 
  case class InitializeSchema(name: String)
  case class GetDatasetQuery(name: String) 
  case object GetDatasetQueries
  case class CancelDatasetQuery(name: String) 
  case class StartDatasetQuery(name: String) 
  case class StopDatasetQuery(name: String)
  case class GetDatasetQueryResult(name: String)
  case object Cancel
  // case class JobDrivers(query_name: String, job1: String, job2: String, job3: String)


  case class DatasetQuery(name: String, event: String, query_type: String, attribute: String, 
    lowerTime: String ="", upperTime: String ="", trendType: String ="", value: String ="",
    running: Boolean = false, job_id: String ="")
  case class DatasetQueries(queries: Vector[DatasetQuery] = Vector.empty[DatasetQuery]) {
    def query_num: Int = { return queries.size }
  }

  sealed trait DatasetQueryResponse 
  case class DatasetQueryCreated(datasetQuery: DatasetQuery) extends DatasetQueryResponse 
  case object DatasetQueryError extends DatasetQueryResponse 
  case object DatasetQueryExists extends DatasetQueryResponse 

  case class DatasetQueryResult(value: String)

}

class DatasetServingLayerSupervisor(name: String) extends Actor {
// class ServingLayerSupervisor(implicit timeout: Timeout) extends Actor {

  import DatasetServingLayerSupervisor._
  import context._

  var event = ""
  var query_type = ""
  var attribute = ""

  def createDatasetQueryActor(name: String) =
    context.actorOf(DatasetQueryActor.props(name), name) 

  def intialize_loadQueries() = {
    val cassandraClient = new CassandraClient()
    cassandraClient.createSchema
    val querySet = cassandraClient.getQueries(query_type,attribute) //event, query_type, attribute
    for (q <- querySet) {
      var query_name = q.getString("query_name")
      var event_name = q.getString("event_name") 
      var query_type = q.getString("query_type")       
      var attribute = q.getString("attribute")
      var lower_time = q.getString("lower_time")
      var upper_time = q.getString("upper_time")
      var trend_type = q.getString("trend_type")
      var trend_value = q.getString("trend_value")
      var running = q.getBool("running")
      var job_id = q.getString("job_id")
      self ! CreateDatasetQuery(query_name, lower_time, upper_time, 
	trend_type, trend_value, running, job_id, true) 
    }
    cassandraClient.close
  }

  override def preStart() {
    // intialize_loadQueries
    // println(s"........ ServingLayerActor (${name}) is started with Actor Reference ${context.self.path}.")
    println(s"........ DatasetServingLayerActor is created as ${context.self}.")
    //println(s"I'm serving $attribute ####################")
  }

 
  def receive = {

    case Event(e) =>  
      event = e
      println(s"........ $name is serving $event event.")

   case QueryType(q_type) => 
      query_type = q_type
      println(s"........ $name is serving $query_type.")

    case Attribute(attr) =>  
	    attribute = attr
      intialize_loadQueries
      println(s"........ $name is serving $attribute.")

    case InitializeSchema(name) =>     
      println(s"${event}: Query schema is created for $name.")
      println("\n------------------------------------------------\n")
      val cassandraClient = new CassandraClient()
      cassandraClient.createDatasetQuerySchema(name, attribute)
      cassandraClient.close

    case CreateDatasetQuery(name, lowerTime, upperTime, trendType, value, running, job_id, loaded) =>
      def create() = {  
        val datasetQueryActor = createDatasetQueryActor(name)
        datasetQueryActor ! DatasetQueryActor.Event(event)
        datasetQueryActor ! DatasetQueryActor.QueryType(query_type)
        datasetQueryActor ! DatasetQueryActor.Attribute(attribute)
        datasetQueryActor ! DatasetQueryActor.LowerTime(lowerTime)
        datasetQueryActor ! DatasetQueryActor.UpperTime(upperTime)
        datasetQueryActor ! TrendQueryActor.TrendType(trendType)
        datasetQueryActor ! TrendQueryActor.Value(value) 
        datasetQueryActor ! DatasetQueryActor.Running(running) 
        datasetQueryActor ! DatasetQueryActor.SetJobId(job_id)
        if (!loaded){
          self ! InitializeSchema(name)
          datasetQueryActor ! DatasetQueryActor.SaveDatasetQuery 
        }
        sender() ! DatasetQueryCreated(DatasetQuery(name, event, query_type, attribute, 
          lowerTime, upperTime, trendType, value, running, job_id))
        println(s"${event}: Creating DatasetQueryActor as (${name}) with Actor Reference ${datasetQueryActor}")
        println(s"${event}: DatasetQueryActor's attribute is (${attribute})")
      }
      context.child(name).fold(create())(_ => sender() ! DatasetQueryExists) 


    case GetDatasetQuery(name) =>
      def notFound() = sender() ! None
      def getDatasetQuery(child: ActorRef) = child forward DatasetQueryActor.GetDatasetQuery
      context.child(name).fold(notFound())(getDatasetQuery)

    
    case GetDatasetQueries =>
      import akka.pattern.ask
      import akka.pattern.pipe
      def getDatasetQueries = context.children.map { child =>
        self.ask(GetDatasetQuery(child.path.name)).mapTo[Option[DatasetQuery]]
      }
      def convertToDatasetQueries(f: Future[Iterable[Option[DatasetQuery]]]) =
        f.map(_.flatten).map(l=> DatasetQueries(l.toVector)) 
      pipe(convertToDatasetQueries(Future.sequence(getDatasetQueries))) to sender()
    

    case CancelDatasetQuery(name) =>
      def notFound() = sender() ! None
      def cancelDatasetQuery(child: ActorRef) = child forward DatasetQueryActor.CancelQuery
      context.child(name).fold(notFound())(cancelDatasetQuery)


    case StartDatasetQuery(name) =>
      def notFound() = sender() ! None
      def startDatasetQuery(child: ActorRef) = child forward DatasetQueryActor.StartQuery
      context.child(name).fold(notFound())(startDatasetQuery)
   

    case StopDatasetQuery(name) =>
      def notFound() = sender() ! None
      def stopDatasetQuery(child: ActorRef) = child forward DatasetQueryActor.StopQuery
      context.child(name).fold(notFound())(stopDatasetQuery)


    case GetDatasetQueryResult(name) =>
      def notFound() = sender() ! None
      def getDatasetQueryResult(child: ActorRef) = child forward DatasetQueryActor.GetResult
      context.child(name).fold(notFound())(getDatasetQueryResult)


    case Cancel =>
      //Cancell all query children
      import akka.pattern.ask
      import akka.pattern.pipe
      def cancelDatasetQueries = context.children.map { child =>
        self.ask(CancelDatasetQuery(child.path.name)).mapTo[Option[DatasetQuery]]
      }
      def convertToDatasetQueries(f: Future[Iterable[Option[DatasetQuery]]]) =
        f.map(_.flatten).map(l=> DatasetQueries(l.toVector)) 
      pipe(convertToDatasetQueries(Future.sequence(cancelDatasetQueries))) to sender()
      self ! PoisonPill      


  }
  
}

