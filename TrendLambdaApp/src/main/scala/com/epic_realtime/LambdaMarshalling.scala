package com.epic_realtime

import spray.json._

case class QueryDescription(trendType: String ="", value: String ="", lowerTime: String="", upperTime: String="") 

case class DatasetQueryDescription(lowerTime: String="", upperTime: String="") 

case class MassageEntry(msg: String) 

case class Error(message: String) 

trait LambdaMarshalling extends DefaultJsonProtocol {
  import LambdaSupervisor._
  import ServingLayerSupervisor._
  import DatasetServingLayerSupervisor._

  implicit val trendLambdaFormat = jsonFormat7(LambdaSupervisor.TrendLambda)
  implicit val trendLambdasFormat = jsonFormat1(LambdaSupervisor.TrendLambdas)
  
  implicit val queryDescriptionFormat = jsonFormat4(QueryDescription)
  implicit val datasetQueryDescriptionFormat = jsonFormat2(DatasetQueryDescription)

  implicit val trendQueryFormat = jsonFormat10(ServingLayerSupervisor.TrendQuery)
  implicit val trendQueriesFormat = jsonFormat1(ServingLayerSupervisor.TrendQueries)

  implicit val datasetQueryFormat = jsonFormat10(DatasetServingLayerSupervisor.DatasetQuery)
  implicit val datasetQueriesFormat = jsonFormat1(DatasetServingLayerSupervisor.DatasetQueries)

  implicit val trendQueryResultFormat = jsonFormat2(ServingLayerSupervisor.TrendQueryResult)
  implicit val datasetQueryResultFormat = jsonFormat1(DatasetServingLayerSupervisor.DatasetQueryResult)

  implicit val massageEntryFormat = jsonFormat1(MassageEntry)
  implicit val errorFormat = jsonFormat1(Error)

 }
