package com.epic_realtime.utils

import com.datastax.driver.core.Cluster
import com.datastax.driver.core.Session
import com.datastax.driver.core.Row
import com.datastax.driver.core.ResultSet
import com.datastax.driver.core.ResultSetFuture
import com.datastax.driver.core.querybuilder.QueryBuilder
import com.datastax.driver.core.querybuilder.QueryBuilder._
import com.datastax.driver.core.Metadata
import scala.collection.JavaConversions._
import com.typesafe.config.{ Config, ConfigFactory } 
// import java.text.SimpleDateFormat
// import com.github.nscala_time.time.Imports._

/**
 * Simple cassandra client, following the datastax documentation
 * (http://www.datastax.com/documentation/developer/java-driver/2.0/java-driver/quick_start/qsSimpleClientCreate_t.html).
 */
class CassandraClient() {

  val config = ConfigFactory.load() 
  val node = config.getString("cassandra.host") 

  // Connect directly to Cassandra from the driver
  private val cluster = Cluster.builder().addContactPoint(node).build()
  // log(cluster.getMetadata())
  private val session = cluster.connect()
  println(s"Connected to cluster ${cluster.getMetadata().getClusterName()}")

  // private def log(metadata: Metadata): Unit = {
  //   Logger.info(s"Connected to cluster: ${metadata.getClusterName}")
  //   for (host <- metadata.getAllHosts()) {
  //     Logger.info(s"Datatacenter: ${host.getDatacenter()}; Host: ${host.getAddress()}; Rack: ${host.getRack()}")
  //   }
  // }

  def createSchema(): Unit = {
    // Execute statements to create two new tables if not exist

    // lambdas Table
    // =============
    session.execute(
      """CREATE TABLE IF NOT EXISTS epic_realtime.lambdas (
        event_name text,
        query_type text,
        attribute text, 
        speed_job_running boolean,
        batch_job_running boolean,        
        speed_job_id text,
        batch_job_id text,
        PRIMARY KEY (event_name, query_type, attribute));""")
    session.execute(
      """CREATE INDEX IF NOT EXISTS lambdas_query_type ON epic_realtime.lambdas (query_type);""")

    // queries Table
    // =============
    session.execute(
      """CREATE TABLE IF NOT EXISTS epic_realtime.queries (
        query_name text PRIMARY KEY,
        event_name text,
        query_type text,
        attribute text,
        lower_time text,
        upper_time text,
        trend_type text,
        trend_value text,
        running boolean,
        job_id text);""")
    session.execute(
      """CREATE INDEX IF NOT EXISTS queries_event_name ON epic_realtime.queries (event_name);""")
    session.execute(
      """CREATE INDEX IF NOT EXISTS queries_query_type ON epic_realtime.queries (query_type);""")
    session.execute(
      """CREATE INDEX IF NOT EXISTS queries_attribute ON epic_realtime.queries (attribute);""")

    // Trend Hashtag
    // =============
    session.execute(
      """CREATE TABLE IF NOT EXISTS epic_realtime.hashtags_realtime (
        hashtag text,
        time timestamp,
        event_name text, 
        PRIMARY KEY (hashtag, time, event_name));""")
    session.execute(
      """CREATE INDEX IF NOT EXISTS hashtags_realtime_event_name ON epic_realtime.hashtags_realtime (event_name);""")
    session.execute(
      """CREATE TABLE IF NOT EXISTS epic_realtime.hashtags_batch (
	      hashtag text,
        time timestamp,
        event_name text,
        PRIMARY KEY (hashtag, time, event_name));""")
    session.execute(
      """CREATE INDEX IF NOT EXISTS hashtags_batch_event_name ON epic_realtime.hashtags_batch (event_name);""")
 
    //Trend URL
    //=============
    session.execute(
      """CREATE TABLE IF NOT EXISTS epic_realtime.urls_realtime (
        url text, 
        time timestamp, 
        event_name text, 
        PRIMARY KEY (url, time, event_name));""")
    session.execute(
      """CREATE INDEX IF NOT EXISTS urls_realtime_event_name ON epic_realtime.urls_realtime (event_name);""")
    session.execute(
      """CREATE TABLE IF NOT EXISTS epic_realtime.urls_batch (
        url text, 
        time timestamp, 
        event_name text, 
        PRIMARY KEY (url, time, event_name));""")
    session.execute(
      """CREATE INDEX IF NOT EXISTS urls_batch_event_name ON epic_realtime.urls_batch (event_name);""")

    //Trend User
    //=============
    session.execute(
      """CREATE TABLE IF NOT EXISTS epic_realtime.users_realtime (
        user text, 
        screen_name text,
        time timestamp, 
        event_name text, 
        PRIMARY KEY (user, time, event_name));""")
    session.execute(
      """CREATE INDEX IF NOT EXISTS users_realtime_event_name ON epic_realtime.users_realtime (event_name);""")
    session.execute(
      """CREATE TABLE IF NOT EXISTS epic_realtime.users_batch (
        user text, 
        screen_name text,
        time timestamp, 
        event_name text, 
        PRIMARY KEY (user, time, event_name));""")
    session.execute(
      """CREATE INDEX IF NOT EXISTS users_batch_event_name ON epic_realtime.users_batch (event_name);""")

    //Trend Mention
    //=============
    session.execute(
      """CREATE TABLE IF NOT EXISTS epic_realtime.mentions_realtime (
        mention text, 
        time timestamp, 
        event_name text, 
        PRIMARY KEY (mention, time, event_name));""")
    session.execute(
      """CREATE INDEX IF NOT EXISTS mentions_realtime_event_name ON epic_realtime.mentions_realtime (event_name);""")
    session.execute(
      """CREATE TABLE IF NOT EXISTS epic_realtime.mentions_batch (
        mention text, 
        time timestamp, 
        event_name text, 
        PRIMARY KEY (mention, time, event_name));""")
    session.execute(
      """CREATE INDEX IF NOT EXISTS mentions_batch_event_name ON epic_realtime.mentions_batch (event_name);""")
  }


  def getEvent(event_name: String): Long = {
    session.execute("SELECT count(*) FROM epic_realtime.events WHERE event_name ='" + event_name + "';").one.getLong(0)
  }


  def createTrendQuerySchema(query_name: String, attribute: String): Unit = {

    //Trend Query
    //=============
    session.execute(
      s"""CREATE TABLE IF NOT EXISTS epic_realtime.${query_name}(
        ${attribute} text, 
        count int, 
        PRIMARY KEY (${attribute}, count));""")
  }


  def createDatasetQuerySchema(query_name: String, attribute: String): Unit = {

    //Unique Query
    //=============
    session.execute(
      s"""CREATE TABLE IF NOT EXISTS epic_realtime.${query_name}(
        ${attribute} text, 
        PRIMARY KEY (${attribute}));""")
  }




  def getAsyncQueryResult(table_name: String, max: Int = 100) : ResultSetFuture = {
    session.executeAsync(QueryBuilder.select().all().from("epic_realtime", table_name).limit(max))
  }



  def addLambda(event_name: String, query_type: String, attribute: String, 
    speed_job_running: Boolean, batch_job_running: Boolean, 
    speed_job_id: String, batch_job_id: String) = {
    session.execute(
      "INSERT INTO epic_realtime.lambdas (event_name, query_type, attribute, speed_job_running, batch_job_running, speed_job_id, batch_job_id) " +
      "VALUES (" +
        "'" + event_name + "'," +
        "'" + query_type + "'," +
        "'" + attribute + "'," +
        speed_job_running + "," +
        batch_job_running + "," +
        "'" + speed_job_id + "'," +
        "'" + batch_job_id + "'" +
        ");")
  }
  def setLambdaSpeedStatus(event_name: String, query_type: String, attribute: String, speed_job_running: Boolean) = {
    session.execute("UPDATE epic_realtime.lambdas SET speed_job_running = " + speed_job_running + 
      " WHERE event_name ='" + event_name + "' AND query_type ='" + query_type + "' AND attribute ='" + attribute + "';")
  }
  def setLambdaBatchStatus(event_name: String, query_type: String, attribute: String, batch_job_running: Boolean) = {
    session.execute("UPDATE epic_realtime.lambdas SET batch_job_running = " + batch_job_running + 
      " WHERE event_name ='" + event_name + "' AND query_type ='" + query_type + "' AND attribute ='" + attribute + "';")
  }
  def getLambdaSpeedStatus(event_name: String, query_type: String, attribute: String): Boolean = {
    session.execute("SELECT speed_job_running FROM epic_realtime.lambdas" +
    " WHERE event_name ='" + event_name + "' AND query_type ='" + query_type + "' AND attribute ='" + attribute + "';").one.getBool(0)
  }
  def getLambdaBatchStatus(event_name: String, query_type: String, attribute: String): Boolean = {
    session.execute("SELECT batch_job_running FROM epic_realtime.lambdas" +
    " WHERE event_name ='" + event_name + "' AND query_type ='" + query_type + "' AND attribute ='" + attribute + "';").one.getBool(0)
  }  
  def deleteLambda(event_name: String, query_type: String, attribute: String) = {
    session.execute("DELETE FROM epic_realtime.lambdas" +
    " WHERE event_name ='" + event_name + "' AND query_type ='" + query_type + "' AND attribute ='" + attribute + "';")
  }
  def getTrendLambdas: ResultSet = {
    session.execute("SELECT * FROM epic_realtime.lambdas WHERE query_type ='trend';")
  }
  def setLambdaSpeedJobId(event_name: String, query_type: String, attribute: String, id: String) = {
    session.execute("UPDATE epic_realtime.lambdas SET speed_job_id = '" + id + 
    "' WHERE event_name ='" + event_name + "' AND query_type ='" + query_type + "' AND attribute ='" + attribute + "';")
  }
  def setLambdaBatchJobId(event_name: String, query_type: String, attribute: String, id: String) = {
    session.execute("UPDATE epic_realtime.lambdas SET batch_job_id = '" + id + 
    "' WHERE event_name ='" + event_name + "' AND query_type ='" + query_type + "' AND attribute ='" + attribute + "';")
  }
  def getLambdaSpeedJobId(event_name: String, query_type: String, attribute: String): String = {
    session.execute("SELECT speed_job_id FROM epic_realtime.lambdas" +
    " WHERE event_name ='" + event_name + "' AND query_type ='" + query_type + "' AND attribute ='" + attribute + "';").one.getString(0)
  }
  def getLambdaBatchJobId(event_name: String, query_type: String, attribute: String): String = {
    session.execute("SELECT batch_job_id FROM epic_realtime.lambdas" +
    " WHERE event_name ='" + event_name + "' AND query_type ='" + query_type + "' AND attribute ='" + attribute + "';").one.getString(0)
  }


  def addQuery(query_name: String, event_name: String, query_type: String, attribute: String, 
    lower_time: String, upper_time: String, trend_type: String, trend_value: String, running: Boolean, job_id: String) = {
    session.execute(
      "INSERT INTO epic_realtime.queries (query_name, event_name, query_type, attribute, lower_time, upper_time, " +
      "trend_type, trend_value, running, job_id) " +
      "VALUES (" +
        "'" + query_name + "'," +
        "'" + event_name + "'," +
        "'" + query_type + "'," +
        "'" + attribute + "'," +
        "'" + lower_time + "'," +
        "'" + upper_time + "'," +
        "'" + trend_type + "'," +
        "'" + trend_value + "'," +
        running + "," +
        "'" + job_id + "'" +
        ");")
  }
  def setQueryStatus(query_name: String, running: Boolean) = {
    session.execute("UPDATE epic_realtime.queries SET running = " + running + " WHERE query_name ='" + query_name + "';")
  }
  def getQuery(query_name: String): Row = {
    session.execute("SELECT * FROM epic_realtime.queries WHERE query_name ='" + query_name + "';").one
  }
  def deleteQuery(query_name: String) = {
    session.execute("DELETE FROM epic_realtime.queries WHERE query_name ='" + query_name + "';")
  }
  def getQueries(query_type: String, attribute: String): ResultSet = {
    session.execute("SELECT * FROM epic_realtime.queries WHERE query_type ='" + query_type + "' AND attribute ='" + attribute + "' ALLOW FILTERING;")
  }
  def setQueryJobId(query_name: String, id: String) = {
    session.execute("UPDATE epic_realtime.queries SET job_id = '" + id + "' WHERE query_name ='" + query_name + "';")
  }
  def getQueryJobId(query_name: String): String = {
    session.execute("SELECT job_id FROM epic_realtime.queries WHERE query_name ='" + query_name + "';").one.getString(0)
  }

  def close() {
    session.close
    cluster.close
  }

}
