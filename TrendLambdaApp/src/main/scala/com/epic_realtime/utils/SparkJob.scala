
package com.epic_realtime.utils

import scala.sys.process._
import play.api.libs.json._
import com.typesafe.config.{ Config, ConfigFactory } 


object SparkJob {

	val config = ConfigFactory.load() 
	val spark_rest = config.getString("spark.rest") 
    val spark_url = config.getString("spark.url") 
	val spark_version = config.getString("spark.version") 


	def runLambda(jarfile: String, appname: String, mainclass:String, 
		event:String, attribute: String, layer: String) : String = {

		val data = """{
	      	"action" : "CreateSubmissionRequest", 
	      	"appArgs" : ["""" + event + 
	      	""""], "appResource" : """" + jarfile + 
	     	"""", "clientSparkVersion" : """" + spark_version + """", 
	     	"environmentVariables" : {"SPARK_ENV_LOADED" : "1"},
			"mainClass" : """" + mainclass +
			"""", "sparkProperties" : {
				"spark.jars" : """" + jarfile + 
				"""", "spark.driver.supervise" : "true", 
				"spark.app.name" : """" + appname +
				"""", "spark.eventLog.enabled" : "true", 
				"spark.submit.deployMode" : "cluster", 
				"spark.driver.allowMultipleContexts" : "true", 
				"spark.master" : """" + spark_rest +
				""""}
			}"""
		val containerJson: JsValue = Json.parse(data)
	    println(containerJson)
	    println(s"${event}: Starting a spark job: (${layer} layer)")
		val deploy = Seq("curl", "-X", 
			"POST", s"${spark_url}/v1/submissions/create", 
			"-H", "Content-Type:application/json;charset=UTF-8", 
			"-d", s"$containerJson")
		// println(deploy.!) 
		val outFile = s"deploy_${event}_${attribute}_${layer}_layer.txt"
	    deploy #> new java.io.File(outFile) !
        val lines = scala.io.Source.fromFile(outFile).mkString
        val driver_id = lines.substring(lines.indexOf("driver"),lines.indexOf("driver")+26)
        // println(outFile)
	    println(s"${event}: Starting a spark job ${mainclass} with driver no. ${driver_id}")
        return driver_id
	}


	def runQuery(jarfile: String, appname: String, mainclass:String,
		event:String, query_name: String, trendType :String, value :String, lowerTime :String, upperTime :String) : String = {
	
		val data = """{
	      	"action" : "CreateSubmissionRequest", 
	      	"appArgs" : ["""" + event + 
	      	"""", """" + query_name +
	      	"""", """" + trendType +
	      	"""", """" + value +
	      	"""", """" + lowerTime +
	      	"""", """" + upperTime +
	      	""""], "appResource" : """" + jarfile + 
	     	"""", "clientSparkVersion" : """" + spark_version + """", 
	     	"environmentVariables" : {"SPARK_ENV_LOADED" : "1"},
			"mainClass" : """" + mainclass +
			"""", "sparkProperties" : {
				"spark.jars" : """" + jarfile + 
				"""", "spark.driver.supervise" : "false", 
				"spark.app.name" : """" + appname +
				"""", "spark.eventLog.enabled" : "true", 
				"spark.submit.deployMode" : "cluster", 
				"spark.driver.allowMultipleContexts" : "true", 
				"spark.master" : """" + spark_rest +
				""""}
			}"""
		val containerJson: JsValue = Json.parse(data)
	    // println(containerJson)
		val deploy = Seq("curl", "-X", 
			"POST", s"${spark_url}/v1/submissions/create", 
			"-H", "Content-Type:application/json;charset=UTF-8", 
			"-d", s"$containerJson")
	    // println(deploy.!) 
	    val outFile = s"deploy_${query_name}_query.txt"
	    deploy #> new java.io.File(outFile) !
        val lines = scala.io.Source.fromFile(outFile).mkString
        val driver_id = lines.substring(lines.indexOf("driver"),lines.indexOf("driver")+26)
        // println(outFile)
	    println(s"${event}: Starting a spark job ${mainclass} with driver no. ${driver_id}")
        return driver_id
	}


	def runDatasetQuery(jarfile: String, appname: String, mainclass:String,
		event:String, query_name: String, lowerTime :String, upperTime :String) : String = {
	
		val data = """{
	      	"action" : "CreateSubmissionRequest", 
	      	"appArgs" : ["""" + event + 
	      	"""", """" + query_name +
	      	"""", """" + lowerTime +
	      	"""", """" + upperTime +
	      	""""], "appResource" : """" + jarfile + 
	     	"""", "clientSparkVersion" : """" + spark_version + """", 
	     	"environmentVariables" : {"SPARK_ENV_LOADED" : "1"},
			"mainClass" : """" + mainclass +
			"""", "sparkProperties" : {
				"spark.jars" : """" + jarfile + 
				"""", "spark.driver.supervise" : "false", 
				"spark.app.name" : """" + appname +
				"""", "spark.eventLog.enabled" : "true", 
				"spark.submit.deployMode" : "cluster", 
				"spark.driver.allowMultipleContexts" : "true", 
				"spark.master" : """" + spark_rest +
				""""}
			}"""
		val containerJson: JsValue = Json.parse(data)
	    // println(containerJson)
		val deploy = Seq("curl", "-X", 
			"POST", s"${spark_url}/v1/submissions/create", 
			"-H", "Content-Type:application/json;charset=UTF-8", 
			"-d", s"$containerJson")
	    // println(deploy.!) 
	    val outFile = s"deploy_${query_name}_query.txt"
	    deploy #> new java.io.File(outFile) !
        val lines = scala.io.Source.fromFile(outFile).mkString
        val driver_id = lines.substring(lines.indexOf("driver"),lines.indexOf("driver")+26)
        // println(outFile)
	    println(s"${event}: Starting a spark job ${mainclass} with driver no. ${driver_id}")
        return driver_id
	}


	def killJob(event:String, driverId: String) {
	  val deploy = Seq("curl", "-X", 
		"POST", s"${spark_url}/v1/submissions/kill/${driverId}")
	  deploy.!
	  println(s"${event}: Killing a spark job of driver no. ${driverId}")
	}

}

 // curl http://spark-cluster-ip:6066/v1/submissions/status/driver-20151008145126-0000

// http://arturmkrtchyan.com/apache-spark-hidden-rest-api

// val deploy = Seq("curl", "-X", "POST", s"$ip/v2/apps", "-H", "Content-Type: application/json", "-d", s"@$containerJson")
// println(deploy.!!) 
// http://alvinalexander.com/scala/how-to-redirect-stdout-stdin-external-commands-in-scala
// http://stackoverflow.com/questions/1284423/read-entire-file-in-scala

//curl -X POST http://localhost:6066/v1/submissions/create --header "Content-Type:application/json;charset=UTF-8" --data '{"action" : "CreateSubmissionRequest", "appArgs" : [ "" ], "appResource" : "/Users/shjambi/spark-launcher-master/spark_app/target/scala-2.10/spark_app-assembly-1.0.jar", "clientSparkVersion" : "1.6.1", "environmentVariables" : {"SPARK_ENV_LOADED" : "1"}, "mainClass" : "SparkApp", "sparkProperties" : {"spark.jars" : "/Users/shjambi/spark-launcher-master/spark_app/target/scala-2.10/spark_app-assembly-1.0.jar", "spark.driver.supervise" : "false", "spark.app.name" : "SparkApp", "spark.eventLog.enabled": "true", "spark.submit.deployMode" : "cluster", "spark.driver.allowMultipleContexts": "true", "spark.master" : "spark://localhost:6066", "spark.master.rest.enabled" : "true"}}'

//curl -X POST http://localhost:6066/v1/submissions/create --header "Content-Type:application/json;charset=UTF-8" --data '{"action" : "CreateSubmissionRequest", "appArgs" : [ "" ], "appResource" : "/Users/shjambi/spark-launcher-master/spark_app/target/scala-2.10/spark_app-assembly-1.0.jar", "clientSparkVersion" : "1.6.1", "environmentVariables" : {"SPARK_ENV_LOADED" : "1"}, "mainClass" : "SparkApp", "sparkProperties" : {"spark.jars" : "/Users/shjambi/spark-launcher-master/spark_app/target/scala-2.10/spark_app-assembly-1.0.jar", "spark.driver.supervise" : "false", "spark.app.name" : "SparkApp", "spark.eventLog.enabled": "true", "spark.submit.deployMode" : "cluster", "spark.master" : "spark://localhost:6066"}}'

//curl -X POST http://localhost:6066/v1/submissions/create --header "Content-Type:application/json;charset=UTF-8" --data '{"action" : "CreateSubmissionRequest", "appArgs" : [ "" ], "appResource" : "/Users/shjambi/spark-launcher-master/spark_app/target/scala-2.10/spark_app-assembly-1.0.jar", "clientSparkVersion" : "1.6.1", "environmentVariables" : {"SPARK_ENV_LOADED" : "1"}, "mainClass" : "SparkApp", "sparkProperties" : {"spark.jars" : "/Users/shjambi/spark-launcher-master/spark_app/target/scala-2.10/spark_app-assembly-1.0.jar", "spark.driver.supervise" : "false", "spark.app.name" : "SparkApp", "spark.eventLog.enabled": "true", "spark.submit.deployMode" : "cluster", "spark.driver.allowMultipleContexts": "true", "spark.master" : "spark://localhost:6066"}}'




