package com.epic_realtime
import akka.actor.{ Actor, Props, PoisonPill }

import scala.concurrent.duration._
import scala.concurrent.Future
import akka.util.Timeout
import akka.pattern.pipe
import com.epic_realtime.utils.SparkJob
import com.epic_realtime.utils.CassandraClient
import com.datastax.driver.core.Row
import com.typesafe.config.{ Config, ConfigFactory } 
import com.epic_realtime.utils.CassandraUtils.resultset._
import scala.collection.JavaConversions._
import java.io._

import ServingLayerSupervisor.TrendQueryResult

object TrendQueryActor {
  def props(name: String) = Props(new TrendQueryActor(name))
 
  case class Event(name: String = "")
  case class QueryType(q_type: String = "")
  case class Attribute(attr: String = "")
  case class LowerTime(time: String = "")
  case class UpperTime(time: String = "")
  case class TrendType(t: String = "")
  case class Value(v: String = "")
  case class Running(s: Boolean)
  case class SetJobId(job: String)
  case object SaveTrendQuery
  case object GetTrendQuery 
  case object CancelQuery 
  case object StartQuery
  case object StopQuery
  case object GetResult
  case object RunQuery

}


class TrendQueryActor(name: String) extends Actor {
  import TrendQueryActor._
  import context._

  var event = ""
  var query_type = ""
  var attribute = ""
  var lowerTime = ""
  var upperTime = ""
  var trendType = ""
  var value = ""
  var running = false
  var job_id = ""

  def buildHashtagRow(r: Row): TrendQueryResult = {
    val value = r.getString("hashtag")
    val count = r.getInt("count")
    TrendQueryResult(value, count)
  }

  def buildUrlRow(r: Row): TrendQueryResult = {
    val value = r.getString("url")
    val count = r.getInt("count")
    TrendQueryResult(value, count)
  }

  def buildUserRow(r: Row): TrendQueryResult = {
    val value = r.getString("user")
    val count = r.getInt("count")
    TrendQueryResult(value, count)
  }

  def buildMentionRow(r: Row): TrendQueryResult = {
    val value = r.getString("mention")
    val count = r.getInt("count")
    TrendQueryResult(value, count)
  }

  val config = ConfigFactory.load() 
  val app_root = config.getString("app.root")
  val app_name = config.getString("app.name")


  def receive = {
   
    case Event(e) =>  event = e

    case QueryType(q_type) => query_type = q_type

    case Attribute(attr) =>  attribute = attr

    case LowerTime(ltime) =>  lowerTime = ltime

    case UpperTime(utime) =>  upperTime = utime

    case TrendType(t) =>  trendType = t

    case Value(v) => value = v

    case Running(s) =>  running = s

    case SetJobId(job) => job_id = job


    case SaveTrendQuery =>
      println(s"${event}: Trend query $name is created.")
      val cassandraClient = new CassandraClient()
      cassandraClient.addQuery(name, event, query_type, attribute, 
        lowerTime, upperTime, trendType, value, running, job_id)
      cassandraClient.close


    case GetTrendQuery => 
      val cassandraClient = new CassandraClient()
      job_id = cassandraClient.getQueryJobId(name)
      sender() ! Some(ServingLayerSupervisor.TrendQuery(name, event, query_type, attribute,
        lowerTime, upperTime, trendType, value, running, job_id)) 


    case CancelQuery => 
      println(s"${event}: Trend query $name is deleted.")
      // self ! StopQuery //a problem of getting the spark job id after the recored has been deleted
      val cassandraClient = new CassandraClient()
      cassandraClient.deleteQuery(name)
      cassandraClient.close
      sender() ! Some(ServingLayerSupervisor.TrendQuery(name, event, query_type, attribute,
        lowerTime, upperTime, trendType, value, running, job_id))
      self ! PoisonPill


    case StartQuery =>
      println(s"${event}: Trend query $name is started.")
      running = true
      println(s"${event}: $name's running status is $running.")
      self ! RunQuery
      // //Retrieve Spark job ids
      //delay?
      val cassandraClient = new CassandraClient()
      job_id = cassandraClient.getQueryJobId(name)
      cassandraClient.setQueryStatus(name, true)      
      cassandraClient.close
      sender() ! Some(ServingLayerSupervisor.TrendQuery(name, event, query_type, attribute,
        lowerTime, upperTime, trendType, value, running, job_id))

      
    case StopQuery =>
      println(s"${event}: Trend query $name is stopped.")
      val cassandraClient = new CassandraClient()
      job_id = cassandraClient.getQueryJobId(name)
      cassandraClient.setQueryStatus(name, false)
      cassandraClient.setQueryJobId(name, "")
      cassandraClient.close
      if (job_id.nonEmpty) SparkJob.killJob(event, job_id)     
      running = false
      job_id = ""
      println(s"${event}: $name's running status is $running.")
      sender() ! Some(ServingLayerSupervisor.TrendQuery(name, event, query_type, attribute,
        lowerTime, upperTime, trendType, value, running, job_id))


    case GetResult =>
      val cassandraClient = new CassandraClient()     
      val resultFuture = cassandraClient.getAsyncQueryResult(name)
      cassandraClient.close

      attribute match{
        case "hashtag" =>
          resultFuture map(_.all().map(buildHashtagRow).toVector.sortBy(-_.count)) pipeTo sender 
        case "url" =>
          resultFuture map(_.all().map(buildUrlRow).toVector.sortBy(-_.count)) pipeTo sender
        case "user" =>
          resultFuture map(_.all().map(buildUserRow).toVector.sortBy(-_.count)) pipeTo sender
        case "mention" =>
          resultFuture map(_.all().map(buildMentionRow).toVector.sortBy(-_.count)) pipeTo sender
      } 
      // import akka.stream.scaladsl.{Source, _}
      // val source = Source.tick(initialDelay = 0.second, interval = 3.second, tick = "tick")
      // Ok.chunked(source.map { tick =>
      //   // lazy val futureList =  epicRealTimeDB.getAll.map(result => Json.toJson(result))//.flatten.mkString(", ")    
      //   val response = Await.result(responseFuture, 100 seconds)
      //   val output_list = (Json.parse(response.body)).as[Seq[TrendQueryResult]]
      //   Json.obj(output_list)
      // })

// val response: HttpResponse = ???
 
// response.entity.dataBytes
//   .via(Framing.delimiter(ByteString("\n"), maximumFrameLength = 256))
//   .map(transformEachLine)
//   .runWith(FileIO.toPath(new File("/tmp/example.out").toPath))
 
// def transformEachLine(line: ByteString): ByteString = ???

// case class ExamplePerson(name: String)
// def parse(line: ByteString): ExamplePerson = ???
 
// val response: HttpResponse = ???
 
// // toStrict to enforce all data be loaded into memory from the connection
// val strictEntity: Future[HttpEntity.Strict] = response.entity.toStrict(3.seconds)
 
// // while API remains the same to consume dataBytes, now they're in memory already:
// val transformedData: Future[ExamplePerson] =
//   strictEntity flatMap { e =>
//     e.dataBytes
//       .runFold(ByteString.empty) { case (acc, b) => acc ++ b }
//       .map(parse)
//   }


    case RunQuery =>
      println(s"${event}: Serving Layer is running ${attribute} trend query $name.")
      val mainclass = "com.epic_realtime.ServingLayer"
      //val jarfile = s"${attribute}/target/scala-2.10/epic_realtime-assembly-1.0.jar"
      val jarfile = s"$attribute/target/scala-2.10/epic_realtime-assembly-1.0.jar"
      if (new File(app_root+jarfile).exists){
        println(s"Found Spark Serving Job file ${app_root+jarfile}")       
        val spark_job_driverid = SparkJob.runQuery(app_root+jarfile, app_name, mainclass, 
          event, name, trendType, value, lowerTime, upperTime)
        val cassandraClient = new CassandraClient()
        cassandraClient.setQueryJobId(name, spark_job_driverid)
        cassandraClient.close       
      } else {
       println(s"Error =====> Spark Serving Job file ${app_root+jarfile} does not exist.")
      }
   

  }
}



