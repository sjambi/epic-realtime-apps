package com.epic_realtime

import scala.concurrent.duration._
import scala.concurrent.ExecutionContext
import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global

import akka.actor._
import akka.pattern.ask
import akka.util.Timeout

import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport._
import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server._

import com.epic_realtime.utils.CassandraClient

import scala.concurrent.{Await, Future}
import scala.concurrent.duration._

case class EventQuery(event: String, attribute: String, id: String)


class RestApi(system: ActorSystem, timeout: Timeout) extends RestRoutes {
  implicit val requestTimeout = timeout
  implicit def executionContext = system.dispatcher

  def createLambdaSupervisor = system.actorOf(LambdaSupervisor.props, LambdaSupervisor.name) 
  def selectServingLayer(event: String, atrribute: String) =
    system.actorSelection(s"/user/lambdaSupervisor/${event}_lambda_${atrribute}/${event}_lambda_${atrribute}_serving")//.resolveOne()

  def selectDatasetServingLayer(event: String, atrribute: String) =
    system.actorSelection(s"/user/lambdaSupervisor/${event}_lambda_${atrribute}/${event}_lambda_${atrribute}_serving_dataset")//.resolveOne()

  // def initialize() = {
  //   val cassandraClient = new CassandraClient("localhost")
  //   cassandraClient.createSchema
  //   cassandraClient.close
  // }

}

trait RestRoutes extends LambdaSupervisorApi
    with LambdaMarshalling {
  import StatusCodes._

  def routes: Route = trendsRoute ~ trendRoute ~ startLambdaRoute ~ stopLambdaRoute ~ trendQueriesRoute ~ trendQueryPostRoute ~ trendQueryGetDelRoute ~ startQueryRoute ~ stopQueryRoute ~ getQueryResultRoute   ~ datasetQueriesRoute ~ datasetQueryPostRoute ~ datasetQueryGetDelRoute ~ datasetStartQueryRoute ~ datasetStopQueryRoute ~ getDatasetQueryResultRoute


  def trendsRoute =
    pathPrefix("events" / Segment / "trend") { event =>
      pathEndOrSingleSlash {
        get {
          // GET /events/:event/trend
           onSuccess(getTrendLambdas(event)) { trendlambdas =>
             complete(OK, trendlambdas)
           }
        }
      }
    }

  def trendRoute =
    pathPrefix("events" / Segment) { event =>
      pathPrefix("trend" / Segment) { attribute =>
        pathEndOrSingleSlash {
          val name = s"${event}_lambda_${attribute}"
          post {
            // POST /events/:event/trend/:attribute
            onSuccess(createTrendLambda(name, event, "trend", attribute)) { 
              case LambdaSupervisor.TrendLambdaCreated(lambda) => complete(Created, lambda) 
              case LambdaSupervisor.TrendLambdaExists =>
                  val err = Error(s"Similar $attribute lambda exists already.")
                  complete(BadRequest, err) 
              case LambdaSupervisor.TrendLambdaNoEventError =>
                  val err = Error(s"Event ${event} is not found.")
                  complete(BadRequest, err) 
            }
          } ~
          get {
            // GET /events/:event/trend/:attribute
            onSuccess(getTrendLambda(name)) {
              _.fold(complete(NotFound))(e => complete(OK, e))
            }
          } ~
          delete {
            // DELETE /events/:event/trend/:attribute
            onSuccess(cancelTrendLambda(name)) {
              _.fold(complete(NotFound))(e => complete(OK, e))
            }
          }
        }
      }
    }

  def startLambdaRoute =
    pathPrefix("events" / Segment) { event =>
      pathPrefix("trend" / Segment / "start") { attribute =>
        pathEndOrSingleSlash {
          post {
            // POST /events/:event/trend/:attribute/start
            onSuccess(startTrendLambda(s"${event}_lambda_${attribute}")) { 
              _.fold(complete(NotFound))(e => complete(OK, e))
            }
          }
        }
      }
    }

  def stopLambdaRoute =
    pathPrefix("events" / Segment) { event =>
      pathPrefix("trend" / Segment / "stop") { attribute =>
        pathEndOrSingleSlash {
          post {
            // POST /events/:event/trend/:attribute/stop
            onSuccess(stopTrendLambda(s"${event}_lambda_${attribute}")) { 
              _.fold(complete(NotFound))(e => complete(OK, e))
            }
          }
        }
      }
    }

  // ===================== Query Routes ======================

  def trendQueriesRoute =
    pathPrefix("events" / Segment) { event =>
      pathPrefix("trend" / Segment / "query" / "all") { attribute =>
        pathEndOrSingleSlash {
          get {
             // GET /events/:event/trend/:attribute/query/all
             onSuccess(getTrendQueries(event, attribute)) { trendqueries =>
               complete(OK, trendqueries)
            }
          }
        }
      }
    }

  def trendQueryPostRoute =
    pathPrefix("events" / Segment) { event =>
      pathPrefix("trend" / Segment / "query") { attribute =>
        pathEndOrSingleSlash {
          post {
            // POST /events/:event/trend/:attribute/query
            entity(as[QueryDescription]) { td => 
              // val name = createEventQueryId(event, attribute)
              onSuccess(createTrendQuery(event, "trend", attribute, td.lowerTime, td.upperTime, td.trendType, td.value)) { 
                case ServingLayerSupervisor.TrendQueryCreated(query) => complete(Created, query) 
                case ServingLayerSupervisor.TrendQueryExists =>
                    val err = Error(s"Similar $attribute query exists already.")
                    complete(BadRequest, err) 
              }
            }
          }
        }
      }
    }

  def trendQueryGetDelRoute =
    pathPrefix("events" / Segment) { event =>
      pathPrefix("trend" / Segment) { attribute =>      
        pathPrefix("query" / Segment) { name =>
          pathEndOrSingleSlash {
            get {
               // GET /events/:event/trend/:attribute/query/:name
              onSuccess(getTrendQuery(name, event, attribute)) {
                _.fold(complete(NotFound))(e => complete(OK, e))
              }
            } ~
            delete {
               // GET /events/:event/trend/:attribute/query/:name
              onSuccess(cancelTrendQuery(name, event, attribute)) {
                _.fold(complete(NotFound))(e => complete(OK, e))
              }
            }
          }
        }
      }
    }

  def startQueryRoute =
    pathPrefix("events" / Segment) { event =>
      pathPrefix("trend" / Segment) { attribute =>      
        pathPrefix("query" / Segment / "start") { name =>
          pathEndOrSingleSlash {
            post {
              // POST /events/:event/trend/:attribute/query/:name/start
              onSuccess(startTrendQuery(name, event, attribute)) { 
                _.fold(complete(NotFound))(e => complete(OK, e))
              }
            }
          }
        }
      }
    }

  def stopQueryRoute =
    pathPrefix("events" / Segment) { event =>
      pathPrefix("trend" / Segment) { attribute =>      
        pathPrefix("query" / Segment / "stop") { name =>
          pathEndOrSingleSlash {
            post {
              // POST /events/:event/trend/:attribute/query/:name/stop
              onSuccess(stopTrendQuery(name, event, attribute)) { 
                _.fold(complete(NotFound))(e => complete(OK, e))
              }
            }
          }
        }
      }
    }
    
  def getQueryResultRoute =
    pathPrefix("events" / Segment) { event =>
      pathPrefix("trend" / Segment) { attribute =>      
        pathPrefix("query" / Segment / "result") { name =>
          pathEndOrSingleSlash {
            get {
              // POST /events/:event/trend/:attribute/query/:name/result
              onSuccess(getTrendQueryResult(name, event, attribute)) { results =>
                complete(OK, results)
              }
            }
          }
        }
      }
    }


  // ===================== Dataset Query Routes ======================

  def datasetQueriesRoute =
    pathPrefix("events" / Segment) { event =>
      pathPrefix("dataset" / Segment / "query" / "all") { attribute =>
        pathEndOrSingleSlash {
          get {
             // GET /events/:event/dataset/:attribute/query/all
             onSuccess(getDatasetQueries(event, attribute)) { datasetqueries =>
               complete(OK, datasetqueries)
            }
          }
        }
      }
    }

  def datasetQueryPostRoute =
    pathPrefix("events" / Segment) { event =>
      pathPrefix("dataset" / Segment / "query") { attribute =>
        pathEndOrSingleSlash {
          post {
            // POST /events/:event/dataset/:attribute/query
            entity(as[DatasetQueryDescription]) { td => 
              // val name = createEventQueryId(event, attribute)
              onSuccess(createDatasetQuery(event, "dataset", attribute, td.lowerTime, td.upperTime)) { 
                case DatasetServingLayerSupervisor.DatasetQueryCreated(query) => complete(Created, query) 
                case DatasetServingLayerSupervisor.DatasetQueryExists =>
                    val err = Error(s"Similar $attribute query exists already.")
                    complete(BadRequest, err) 
              }
            }
          }
        }
      }
    }

  def datasetQueryGetDelRoute =
    pathPrefix("events" / Segment) { event =>
      pathPrefix("dataset" / Segment) { attribute =>      
        pathPrefix("query" / Segment) { name =>
          pathEndOrSingleSlash {
            get {
               // GET /events/:event/dataset/:attribute/query/:name
              onSuccess(getDatasetQuery(name, event, attribute)) {
                _.fold(complete(NotFound))(e => complete(OK, e))
              }
            } ~
            delete {
               // GET /events/:event/dataset/:attribute/query/:name
              onSuccess(cancelDatasetQuery(name, event, attribute)) {
                _.fold(complete(NotFound))(e => complete(OK, e))
              }
            }
          }
        }
      }
    }

  def datasetStartQueryRoute =
    pathPrefix("events" / Segment) { event =>
      pathPrefix("dataset" / Segment) { attribute =>      
        pathPrefix("query" / Segment / "start") { name =>
          pathEndOrSingleSlash {
            post {
              // POST /events/:event/dataset/:attribute/query/:name/start
              onSuccess(startDatasetQuery(name, event, attribute)) { 
                _.fold(complete(NotFound))(e => complete(OK, e))
              }
            }
          }
        }
      }
    }

  def datasetStopQueryRoute =
    pathPrefix("events" / Segment) { event =>
      pathPrefix("dataset" / Segment) { attribute =>      
        pathPrefix("query" / Segment / "stop") { name =>
          pathEndOrSingleSlash {
            post {
              // POST /events/:event/dataset/:attribute/query/:name/stop
              onSuccess(stopDatasetQuery(name, event, attribute)) { 
                _.fold(complete(NotFound))(e => complete(OK, e))
              }
            }
          }
        }
      }
    }
    
  def getDatasetQueryResultRoute =
    pathPrefix("events" / Segment) { event =>
      pathPrefix("dataset" / Segment) { attribute =>      
        pathPrefix("query" / Segment / "result") { name =>
          pathEndOrSingleSlash {
            get {
              // GET /events/:event/dataset/:attribute/query/:name/result
              onSuccess(getDatasetQueryResult(name, event, attribute)) { results =>
                complete(OK, results)
              }
            }
          }
        }
      }
    }

}


trait LambdaSupervisorApi {
  import LambdaSupervisor._
  import ServingLayerSupervisor._
  import DatasetServingLayerSupervisor._

  def createLambdaSupervisor(): ActorRef
  def selectServingLayer(event: String, atrribute: String): ActorSelection
  def selectDatasetServingLayer(event: String, atrribute: String): ActorSelection


  implicit def executionContext: ExecutionContext
  implicit def requestTimeout: Timeout

  lazy val lambdaSupervisor = createLambdaSupervisor()
  println(s"\nLambdaSepervisor is created with Actor Reference ${lambdaSupervisor}.\n")

  // lambdaSupervisor.ask(InitializeSchema)
  // println("\n-----------------------------------------\n")
  // initialize
  // println("SCHEMA CREATED.")
  // println("\n\n-----------------------------------------\n")


  def getTrendLambdas(event: String) =
    lambdaSupervisor.ask(GetTrendLambdas(event))
    .mapTo[TrendLambdas]

  def createTrendLambda(name: String, event: String, query_type: String, attribute: String, 
    speed_job_running: Boolean=false, batch_job_running: Boolean=false,
    speed_job_id: String="", batch_job_id: String="") = {
    lambdaSupervisor.ask(CreateTrendLambda(name, event, query_type, attribute, speed_job_running, batch_job_running, speed_job_id, batch_job_id, false))
      .mapTo[TrendLambdaResponse]
  }

  def getTrendLambda(name: String) =
    lambdaSupervisor.ask(GetTrendLambda(name))
      .mapTo[Option[TrendLambda]]

  def cancelTrendLambda(name: String) =
    lambdaSupervisor.ask(CancelTrendLambda(name))
      .mapTo[Option[TrendLambda]]

 def startTrendLambda(name: String) =
    lambdaSupervisor.ask(StartTrendLambda(name))
      .mapTo[Option[TrendLambda]]

  def stopTrendLambda(name: String) =
    lambdaSupervisor.ask(StopTrendLambda(name))
      .mapTo[Option[TrendLambda]]


  // var EventQueryList = Vector.empty[EventQuery] 

  // def createEventQueryId(event: String, attribute: String) : String = {
  //   var count = EventQueryList.count(q => (q.event == event) && (q.attribute == attribute))
  //   val id = "%03d".format(count+1).toString
  //   EventQueryList = EventQueryList :+ EventQuery(event, attribute, id)
  //   // println("Event Query List:")
  //   // for (q <- EventQueryList) {
  //   //   println(s"${q.event}, ${q.attribute}, ${q.id}")
  //   // }
  //   return s"${event}_${attribute}_${id}"
  // }

  def createTrendQuery(event: String, query_type: String, attribute: String, lowerTime: String, upperTime: String, 
    trendType: String, value: String, running: Boolean=false, job_id: String="") = {
    val servingLayer = selectServingLayer(event, attribute)
    //Create a name for the new query
    val responseFuture = servingLayer ? GetTrendQueries
    val result = Await.result(responseFuture, 10 seconds).asInstanceOf[TrendQueries]
    val query_num = result.query_num
    val id = "%02d".format(query_num+1).toString
    val name = s"${event}_${query_type}_${attribute}_${id}"
    println(s"\n${event}: Calling ServingLayerActor.CreateTrendQuery by ${servingLayer} for creating new query ${name}")
    servingLayer.ask(CreateTrendQuery(name, lowerTime, upperTime, trendType, value, running, job_id, false))
      .mapTo[TrendQueryResponse]

    // val ref = scala.concurrent.Await.result(system.actorSelection("/user/lambdaSupervisor/ONS2016_lambda_user/ONS2016_lambda_user_serving")
      // .resolveOne()(akka.util.Timeout.intToTimeout(1000)), 10.seconds)

    // implicit val timeout = Timeout(5 seconds)
    // val future = servingLayer ? CreateTrendQuery(name, event, attribute, lowerTime, upperTime, trendType, value)
    // val result = Await.result(future, timeout.duration).asInstanceOf[TrendQueryResponse]
    // println(result)

    // val future2: Future[TrendQueryResponse] = ask(servingLayer, CreateTrendQuery(name, event, attribute, lowerTime, upperTime, trendType, value))
    //   .mapTo[TrendQueryResponse]
    // val result2 = Await.result(future2, 1 second)
    // println(result2)
  }

  def getTrendQueries(event: String, attribute: String) = {
    val servingLayer = selectServingLayer(event, attribute)
    println(s"\n${event}: Calling ServingLayerActor.GetTrendQueries by ${servingLayer}")
    servingLayer.ask(GetTrendQueries)
    .mapTo[TrendQueries]
  }

  def getTrendQuery(name: String, event: String, attribute: String) = {
    val servingLayer = selectServingLayer(event, attribute)
    println(s"\n${event}: Calling ServingLayerActor.GetTrendQuery by ${servingLayer}")
    servingLayer.ask(GetTrendQuery(name))
      .mapTo[Option[TrendQuery]]
  }

  def cancelTrendQuery(name: String, event: String, attribute: String) = {
    val servingLayer = selectServingLayer(event, attribute)
    println(s"\n${event}: Calling ServingLayerActor.CancelTrendQuery by ${servingLayer}")
    servingLayer.ask(CancelTrendQuery(name))
      .mapTo[Option[TrendQuery]]
  }

  def startTrendQuery(name: String, event: String, attribute: String) = {
    val servingLayer = selectServingLayer(event, attribute)
    println(s"\n${event}: Calling ServingLayerActor.StartTrendQuery by ${servingLayer}")
    servingLayer.ask(StartTrendQuery(name))
      .mapTo[Option[TrendQuery]]
  }

  def stopTrendQuery(name: String, event: String, attribute: String) = {
    val servingLayer = selectServingLayer(event, attribute)
    println(s"\n${event}: Calling ServingLayerActor.StopTrendQuery by ${servingLayer}")
    servingLayer.ask(StopTrendQuery(name))
      .mapTo[Option[TrendQuery]]
  }

  def getTrendQueryResult(name: String, event: String, attribute: String) = {
    val servingLayer = selectServingLayer(event, attribute)
    println(s"\n${event}: Calling ServingLayerActor.GetTrendQueryResult by ${servingLayer}")
    servingLayer.ask(GetTrendQueryResult(name))
      .mapTo[Vector[ServingLayerSupervisor.TrendQueryResult]]
  }


  //------------Dataset Query
 
    def createDatasetQuery(event: String, query_type: String, attribute: String, lowerTime: String, upperTime: String,
     trendType: String ="", value: String ="", running: Boolean=false, job_id: String="") = {
    val servingLayer = selectDatasetServingLayer(event, attribute)
    //Create a name for the new query
    val responseFuture = servingLayer ? GetDatasetQueries
    val result = Await.result(responseFuture, 10 seconds).asInstanceOf[DatasetQueries]
    val query_num = result.query_num
    val id = "%02d".format(query_num+1).toString
    val name = s"${event}_${query_type}_${attribute}_${id}"
    println(s"\n${event}: Calling DatasetServingLayer.CreateDatasetQuery by ${servingLayer} for creating new query ${name}")
    servingLayer.ask(CreateDatasetQuery(name, lowerTime, upperTime, trendType, value, running, job_id, false))
      .mapTo[DatasetQueryResponse]
  }

  def getDatasetQueries(event: String, attribute: String) = {
    val servingLayer = selectDatasetServingLayer(event, attribute)
    println(s"\n${event}: Calling DatasetServingLayer.GetDatasetQueries by ${servingLayer}")
    servingLayer.ask(GetDatasetQueries)
    .mapTo[DatasetQueries]
  }

  def getDatasetQuery(name: String, event: String, attribute: String) = {
    val servingLayer = selectDatasetServingLayer(event, attribute)
    println(s"\n${event}: Calling DatasetServingLayer.GetDatasetQuery by ${servingLayer}")
    servingLayer.ask(GetDatasetQuery(name))
      .mapTo[Option[DatasetQuery]]
  }

  def cancelDatasetQuery(name: String, event: String, attribute: String) = {
    val servingLayer = selectDatasetServingLayer(event, attribute)
    println(s"\n${event}: Calling DatasetServingLayer.CancelDatasetQuery by ${servingLayer}")
    servingLayer.ask(CancelDatasetQuery(name))
      .mapTo[Option[DatasetQuery]]
  }

  def startDatasetQuery(name: String, event: String, attribute: String) = {
    val servingLayer = selectDatasetServingLayer(event, attribute)
    println(s"\n${event}: Calling DatasetServingLayer.StartDatasetQuery by ${servingLayer}")
    servingLayer.ask(StartDatasetQuery(name))
      .mapTo[Option[DatasetQuery]]
  }

  def stopDatasetQuery(name: String, event: String, attribute: String) = {
    val servingLayer = selectDatasetServingLayer(event, attribute)
    println(s"\n${event}: Calling DatasetServingLayer.StopDatasetQuery by ${servingLayer}")
    servingLayer.ask(StopDatasetQuery(name))
      .mapTo[Option[DatasetQuery]]
  }

  def getDatasetQueryResult(name: String, event: String, attribute: String) = {
    val servingLayer = selectDatasetServingLayer(event, attribute)
    println(s"\n${event}: Calling DatasetServingLayer.GetDatasetQueryResult by ${servingLayer}")
    servingLayer.ask(GetDatasetQueryResult(name))
      .mapTo[Vector[DatasetServingLayerSupervisor.DatasetQueryResult]]
  }


}

