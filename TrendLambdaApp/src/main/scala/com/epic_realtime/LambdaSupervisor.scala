package com.epic_realtime

import scala.concurrent.duration._
import scala.concurrent.Future
import akka.actor._
import akka.util.Timeout
import com.epic_realtime.utils.CassandraClient
import scala.collection.JavaConversions._

object LambdaSupervisor {
  def props(implicit timeout: Timeout) = Props(new LambdaSupervisor)
  def name = "lambdaSupervisor"

  case class TrendLambda(event: String, query_type: String, attribute: String, speed_job_running: Boolean, batch_job_running: Boolean, 
    speed_job_id: String ="", batch_job_id: String ="")  
  case class TrendLambdas(trendLambdas: Vector[TrendLambda] = Vector.empty[TrendLambda]) 

  case object InitializeSchema 
  case class CreateTrendLambda(name: String, event: String, query_type: String, attribute: String, speed_job_running: Boolean, batch_job_running: Boolean,
    speed_job_id: String, batch_job_id: String, loaded: Boolean) 
  case class GetTrendLambda(name: String)
  case class GetTrendLambdas(event: String)
  case class CancelTrendLambda(name: String)
  case class StartTrendLambda(name: String)
  case class StopTrendLambda(name: String)

  sealed trait TrendLambdaResponse 
  case class TrendLambdaCreated(trendLambda: TrendLambda) extends TrendLambdaResponse 
  case object TrendLambdaExists extends TrendLambdaResponse 
  case object TrendLambdaNoEventError extends TrendLambdaResponse 
}

class LambdaSupervisor(implicit timeout: Timeout) extends Actor {
  import LambdaSupervisor._
  import context._

  def createTrendLambdaActor(name: String) =
    context.actorOf(TrendLambdaActor.props(name), name) 

  def intialize_loadLambdas() = {
    val cassandraClient = new CassandraClient()
    cassandraClient.createSchema
    val lambdaSet = cassandraClient.getTrendLambdas //event
    cassandraClient.close
    for (l <- lambdaSet) {
      var event_name = l.getString("event_name")
      var query_type = l.getString("query_type")
      var attribute = l.getString("attribute")
      var speed_job_running = l.getBool("speed_job_running")
      var batch_job_running = l.getBool("batch_job_running")
      var speed_job_id = l.getString("speed_job_id")
      var batch_job_id = l.getString("batch_job_id")
      val name = s"${event_name}_lambda_${attribute}"
      self ! CreateTrendLambda(name, event_name, query_type, attribute, speed_job_running, batch_job_running, speed_job_id, batch_job_id, true)
    }
  }

  def eventExists(event: String): Boolean = {
    val cassandraClient = new CassandraClient()
    val count = cassandraClient.getEvent(event)
    cassandraClient.close
    if (count > 0) return true else return false
  }
  
  override def preStart() {
    // println(s"LambdaSupervisor Actor is started with path ${context.self.path}.")
    intialize_loadLambdas
    println("SCHEMA CREATED.")
    println("\n-----------------------------------------\n")
  }

 
  def receive = {

    case CreateTrendLambda(name, event, query_type, attribute, speed_job_running, batch_job_running, speed_job_id, batch_job_id, loaded) =>
      if (eventExists(event)){ //already created
        def create() = {  
          val trendLambdaActor = createTrendLambdaActor(name)
          trendLambdaActor ! TrendLambdaActor.Event(event)
          trendLambdaActor ! TrendLambdaActor.QueryType(query_type)
          trendLambdaActor ! TrendLambdaActor.Attribute(attribute)
          trendLambdaActor ! TrendLambdaActor.SpeedJobRunning(speed_job_running)
          trendLambdaActor ! TrendLambdaActor.BatchJobRunning(batch_job_running)        
          trendLambdaActor ! TrendLambdaActor.SetSpeedJobIds(speed_job_id)
          trendLambdaActor ! TrendLambdaActor.SetBatchJobIds(batch_job_id)
          if (!loaded) trendLambdaActor ! TrendLambdaActor.SaveTrendLambda
          sender() ! TrendLambdaCreated(TrendLambda(event, query_type, attribute, speed_job_running, batch_job_running, speed_job_id, batch_job_id))
          println(s"\n${event}: Creating TrendLambdaActor as (${name}) with Actor Reference ${trendLambdaActor}")
        }
        context.child(name).fold(create())(_ => sender() ! TrendLambdaExists)
      } else {
        sender() ! TrendLambdaNoEventError
      }  
      
    case GetTrendLambda(name) =>
      def notFound() = sender() ! None
      def getTrendLambda(child: ActorRef) = child forward TrendLambdaActor.GetTrendLambda
      context.child(name).fold(notFound())(getTrendLambda)

    case GetTrendLambdas(event) =>
      //''''''''''''''''''''''''''''''''''''''''''''''''''''''''
      // Should return trend lambdas of the requested event only
      //''''''''''''''''''''''''''''''''''''''''''''''''''''''''  
      import akka.pattern.ask
      import akka.pattern.pipe
      def getTrendLambdas = context.children.map { child =>
        self.ask(GetTrendLambda(child.path.name)).mapTo[Option[TrendLambda]] 
      }
      def convertToTrendLambdas(f: Future[Iterable[Option[TrendLambda]]]) =
        f.map(_.flatten).map(l=> TrendLambdas(l.toVector)) 
      pipe(convertToTrendLambdas(Future.sequence(getTrendLambdas))) to sender() 
 
    case CancelTrendLambda(name) =>
      def notFound() = sender() ! None
      def cancelTrendLambda(child: ActorRef) = child forward TrendLambdaActor.CancelTrendLambda
      context.child(name).fold(notFound())(cancelTrendLambda)

    case StartTrendLambda(name) =>
      def notFound() = sender() ! None
      def startTrendLambda(child: ActorRef) = child forward TrendLambdaActor.Start
      context.child(name).fold(notFound())(startTrendLambda)

    case StopTrendLambda(name) =>
      def notFound() = sender() ! None
      def stopTrendLambda(child: ActorRef) = child forward TrendLambdaActor.Stop
      context.child(name).fold(notFound())(stopTrendLambda)

  }
}

