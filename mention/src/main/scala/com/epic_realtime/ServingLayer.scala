package com.epic_realtime

import com.epic_realtime.SparkCommon._
import com.datastax.spark.connector._
import org.apache.spark.sql._
import org.apache.spark.sql.SQLContext
import org.apache.spark.sql.types._
import org.apache.spark.sql.Row
import org.apache.spark.sql.functions._
import org.apache.spark.sql.types.{StructType,StructField,StringType};
import org.apache.spark.rdd.RDD
import org.apache.spark.streaming.dstream.ConstantInputDStream
import com.github.nscala_time.time.Imports._


object ServingLayer {

  //--------------------------------
  // This is a continuous query type
  //--------------------------------
  def main(args: Array[String]) {
    if (args.length < 2) {
      System.err.println("Usage: com.epic_realtime.ServingLayer <event_name> <query_name> " +
        "<trendtype[threshold/topk] <trendvalue> <start_date[yyyy-MM-dd]> <end_date[yyyy-MM-dd]>")
      System.exit(1)
    }

    var event = args(0)
    var query_name = args(1)
    var trend = if (args.length > 2) args(2) else "topk"   //default value
    var value = if (args.length > 3) args(3) else "10"     //default value
    var start_date = if (args.length > 4) args(4) else "" 
    var end_date = if (args.length > 5) args(5) else "" 

    val conf = SparkCommon.conf(query_name, "1")
    val sc = SparkCommon.sc(conf)
    val ssc = SparkCommon.ssc(sc, 30)
    import java.util.Properties
    import org.apache.kafka.clients.producer.{ProducerRecord, KafkaProducer}
    val topic = query_name
    val producer = new KafkaProducer[String, String](SparkCommon.prop)
    
    // Combine data from two Cassandra tables using union
    val realtimeRDD = sc.cassandraTable("epic_realtime", s"mentions_realtime").where("event_name = ?", event)
    val batchRDD = sc.cassandraTable("epic_realtime", s"mentions_batch").where("event_name = ?", event)
    val unionRDD = batchRDD.union(realtimeRDD)
    // unionRDD.cache()

    // Generate continuous stream of Dstream
    val dstream = new ConstantInputDStream(ssc, unionRDD)
    
    dstream.foreachRDD{ rdd => 

      if (rdd.count>0){

        val d1 = DateTime.now

        val sqlContext = new SQLContext(sc)
        import sqlContext.implicits._
       
        // Generate the schema based on a string of schema
        val schemaString = "time mention"
        val schema = StructType(schemaString.split(" ").map(fieldName => StructField(fieldName, StringType, true)))
        // Convert records of RDD to Rows.
        val rowRDD = rdd.map(r => Row(r.get[String]("time"), r.get[String]("mention")))
        // Apply the schema to the RDD.
        val mentionDF = sqlContext.createDataFrame(rowRDD, schema)
       // userDF.printSchema()

        if (start_date == "") {start_date = DateTime.now.minusDays(1)toString}

        val mentionCount = 
        if (end_date == ""){
          mentionDF
          .filter(to_date(mentionDF("time")).gt(start_date))
          .groupBy("mention").count()
          .sort(desc("count"),asc("mention"))    
        }else{
          mentionDF
          .filter(date_format(to_date(mentionDF("time")), "yyyy-MM-dd").between(start_date, end_date))
          .groupBy("mention").count()
          .sort(desc("count"),asc("mention"))
        }
        //mentionCount.show()

        // Check mention's input
        val mentionTrend =
        trend match {
          case "threshold" => mentionCount.where("count>="+value)
          case "topk" => mentionCount.limit(value.toInt)
          case _ => mentionCount.limit(10)
        }        
        mentionTrend.show()

        val d2 = DateTime.now
        val dateDiff = d2.getMillis()-d1.getMillis()
        producer.send(new ProducerRecord[String, String]
          (topic, s"${realtimeRDD.count}, ${batchRDD.count}, ${realtimeRDD.count+batchRDD.count}, ${unionRDD.count}, ${rdd.count.toString}, ${dateDiff.toString}"))

        mentionTrend.write
          .format("org.apache.spark.sql.cassandra")
          .options(Map( "table" -> s"${query_name.toLowerCase()}", "keyspace" -> "epic_realtime"))
          .mode(SaveMode.Overwrite)
          .save()
      }
    }

    ssc.start()
    // timer.start()
    ssc.awaitTermination()
    // ssc.stop()

  }
}

//http://stackoverflow.com/questions/32451614/reading-from-cassandra-using-spark-streaming
//https://databricks.com/blog/2015/09/16/apache-spark-1-5-dataframe-api-highlights.html
