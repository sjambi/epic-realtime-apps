mention
=======
This app contains three types of Spark jobs that create a Lambda Architecture process for Trend Queries that flexibly find trending mentions in Twitter streaming and historical data of a requested event.


************** Spark Jobs ***************


SpeedLayer
----------
Creates a mention real-time view for the requested event. It consumes Twitter data streaming from event's Kafka topic, extracts mentions found in tweet's text along with the created_at value, then stores a tuple of (event_name, time, id) into a Cassandra table called "mentions_realtime".

com.epic_realtime.SpeedLayer <event_name>

Ex. 
/Users/shjambi/spark-1.6.1-bin-hadoop2.6/bin/spark-submit --class com.epic_realtime.SpeedLayer target/scala-2.10/epic_realtime-assembly-1.0.jar ONS2016



BatchLayer
----------
Creates a mention batch view for the requested event. It retrieves Twitter historical data stored in a Cassandra table called "tweets" that contains all previously collected tweet raw data for all events. It filters in event's tweets, reads the created_at column, extracts mentions from the tweet_json column, then stores a tuple of (event_name, time, id) into a Cassandra table called "mentions_batch".

com.epic_realtime.BatchLayer <event_name>

Ex. 
/Users/shjambi/spark-1.6.1-bin-hadoop2.6/bin/spark-submit --class com.epic_realtime.BatchLayer target/scala-2.10/epic_realtime-assembly-1.0.jar ONS2016



ServingLayer
------------
Creates a mention query result of realtime and batch user mention aggregates values for the requested event. It retrieves real-time and batch user mentions data stored in Cassandra tables "mentions_realtime" and "mentions_batch", merges them, filters data based on a requested time period, then generates a list of trending user mentions with their counts, either by a threshold that determines if a user mention is considered a trend or a topk value of the most frequently posted user mentions.
The output is saved in a new Cassandra table named by the query's generated name, updated every new data streams in. 

com.epic_realtime.ServingLayer <event_name> <query_name> 
<trendtype[threshold/topk] <trendvalue> <start_date[yyyy-MM-dd]> <end_date[yyyy-MM-dd]> 

Note: 
- if trendtype and trendvalue are not provided, the default values are topk 10
- if start_date or/and end_date are not provided, the default values are Now date

Ex. 
/Users/shjambi/spark-1.6.1-bin-hadoop2.6/bin/spark-submit --class com.epic_realtime.ServingLayer target/scala-2.10/epic_realtime-assembly-1.0.jar ONS2016 ONS2016_mention_01 threshold 10 2016-07-02 2016-07-03 



DatasetServingLayer
-------------------
Creates a user mention query result of realtime and batch distinct user mention values for the requested event. It retrieves real-time and batch user mentions data stored in Cassandra tables "mentions_realtime" and "mentions_batch", merges them, filters data based on a requested time period, then generates a list of unique user mentions.
The output is saved in a new Cassandra table named by the query's generated name, updated every new data streams in. 

com.epic_realtime.DatasetServingLayer <event_name> <query_name> 
<start_date[yyyy-MM-dd]> <end_date[yyyy-MM-dd]> 

Note: 
- if start_date or/and end_date are not provided, the default values are Now date

Ex. 
/Users/shjambi/spark-1.6.1-bin-hadoop2.6/bin/spark-submit --class com.epic_realtime.DatasetServingLayer target/scala-2.10/epic_realtime-assembly-1.0.jar ONS2016 ONS2016_mention_01 2016-07-02 2016-07-03 

