enablePlugins(JavaServerAppPackaging)

name := "epic_realtime"

version := "1.0"

organization := "com.epic_realtime"

libraryDependencies ++= {
  val akkaVersion = "2.4.8"
  Seq(
    "com.typesafe.akka" %% "akka-actor"      % akkaVersion,
    "com.typesafe.akka" %% "akka-http-core"  % akkaVersion, 
    "com.typesafe.akka" %% "akka-http-experimental"  % akkaVersion, 
    "com.typesafe.akka" %% "akka-http-spray-json-experimental"  % akkaVersion, 
    "com.typesafe.akka" %% "akka-slf4j"      % akkaVersion,
    "ch.qos.logback"    %  "logback-classic" % "1.1.3",
    "com.typesafe.akka" %% "akka-testkit"    % akkaVersion   % "test",
    "org.scalatest"     %% "scalatest"       % "2.2.4"       % "test",
    "com.typesafe"      % "config"           % "1.2.1",
    "com.typesafe.play" %% "play-json" % "2.4.0-M3",
    "com.github.nscala-time" %% "nscala-time" % "2.12.0",
    //"joda-time"         % "joda-time"        % "2.7",
    "org.apache.kafka"  % "kafka-clients"    % "0.9.0.0",
    "org.twitter4j"     % "twitter4j-core"   % "4.0.2",
    "org.twitter4j"     % "twitter4j-stream" % "4.0.2",
   ("com.datastax.cassandra" % "cassandra-driver-core" % "3.0.3")//.exclude("io.netty", "netty-handler")
    //"org.apache.kafka"  %  "kafka_2.11"     % "0.9.0.0"
    //  exclude("log4j", "log4j") 
    //  exclude("org.slf4j","slf4j-log4j12")
    //  exclude("javax.jms", "jms")
    //  exclude("com.sun.jdmk", "jmxtools")
    //  exclude("com.sun.jmx", "jmxri"),
    //"com.101tec" % "zkclient" % "0.7"
    //("com.datastax.cassandra" % "cassandra-driver-core" % "2.2.0-rc3").exclude("io.netty", //"netty-handler")
  )
}

resolvers += "Typesafe Repo" at "http://repo.typesafe.com/typesafe/releases/"

assemblyMergeStrategy in assembly <<= (assemblyMergeStrategy in assembly) {
        (old) => {
         case PathList("META-INF", "io.netty.versions.properties") => MergeStrategy.last
          case x => old(x)
        }
      }
