scalaVersion := "2.11.8"

scalacOptions ++= Seq(
  "-target:jvm-1.7",
  "-deprecation",
  "-unchecked",
  "-feature",
  "-language:_"
)
