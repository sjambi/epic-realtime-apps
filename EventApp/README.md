EventApp
=========
This app provides REST APIs to create and manage all events in the system.
We can create an event, add keywords, then activate the event to start collecting and persisting Twitter's data for that event. We can also deactivate the event to stop any data collection or persisting, or delete the event from the system.
For every active event, the app collects Twitter data streaming into a Kafka producer topic named by the event, and simultaneously consumes and persists this data stream as historical Twitter data in the "tweets" Cassandra table that contains all previously collected tweet raw data for all events. 


Requiremnets:
- Cassandra v.2.2.6 or higher
- Kafka v. 2.11-0.9.0.0 or higher
- Scala v. 2.10.5 or higher
- Httpie: a human-readable HTTP  command-line tool (optional)
- Set the following varaibles in application.conf:
   - cassandra host
   - kafka server, kafka path, and zookeeper server
 

Running the App:
From the app's root, run the following sbt commands: 
$ sbt assembly
$ sbt run


Using the App:
Go to another tab, and call the app's REST APIs (using Httpie)


An Event Example:
Event Description: 2016 Orlando nightclub shooting
Event Start Date: 6/12/2016
Event Name: ONS2016

Create the event
----------------
http POST localhost:5000/events/ONS2016 startdate=6/12/2016
HTTP/1.1 201 Created
{
    "endDate": "",
    "isActive": false,
    "name": "ONS2016",
    "startDate": "6/12/2016"
}

Add a new keyword
-----------------
http POST localhost:5000/events/ONS2016/keywords word=orlando
HTTP/1.1 201 Created
{
    "text": "orlando"
}
//at least one keyword should be created in order to activate the event

Add a new keyword
-----------------
http POST localhost:5000/events/ONS2016/keywords word=shooting
HTTP/1.1 201 Created
{
    "text": "shooting"
}

Get all keywords
----------------
http GET localhost:5000/events/ONS2016/keywords
HTTP/1.1 200 OK
{
    "keywords": [
        {
            "text": "orlando"
        },
        {
            "text": "shooting"
        }
    ]
}

Delete a keyword
----------------
http DELETE localhost:5000/events/ONS2016/keywords word=shooting
HTTP/1.1 200 OK
{
    "endDate": "",
    "isActive": false,
    "name": "ONS2016",
    "startDate": "6/12/2016"
}

Get an event
------------
http GET localhost:5000/events/ONS2016
HTTP/1.1 200 OK
{
    "endDate": "",
    "isActive": false,
    "name": "ONS2016",
    "startDate": "6/12/2016"
}

Create another event
--------------------
http POST localhost:5000/events/BFF2016 startdate=9/1/2013
HTTP/1.1 201 Created
{
    "endDate": "",
    "isActive": false,
    "name": "BFF2016",
    "startDate": "9/1/2013"
}

Get all events
--------------
http GET localhost:5000/events
HTTP/1.1 200 OK
{
    "events": [
        {
            "endDate": "",
            "isActive": false,
            "name": "BFF2016",
            "startDate": "9/1/2013"
        },
        {
            "endDate": "",
            "isActive": false,
            "name": "ONS2016",
            "startDate": "6/12/2016"
        }
    ]
}

Delete an event
---------------
http DELETE localhost:5000/events/BFF2016
HTTP/1.1 200 OK
{
    "endDate": "",
    "isActive": false,
    "name": "BFF2016",
    "startDate": "9/1/2013"
}

Activate an event
------------------
http POST localhost:5000/events/ONS2016/active active=true
HTTP/1.1 200 OK
{
    "endDate": "",
    "isActive": true,
    "name": "ONS2016",
    "startDate": "6/12/2016"
}

Deactivate an event
--------------------
http POST localhost:5000/events/ONS2016/active active=false
HTTP/1.1 200 OK
{
    "endDate": "2016-07-02T19:46:32.853-06:00",
    "isActive": false,
    "name": "ONS2016",
    "startDate": "6/12/2016"
}

Get Tweet count
----------------
http GET localhost:5000/events/ONS2016/count
HTTP/1.1 200 OK
{
    "count": "76"
}
