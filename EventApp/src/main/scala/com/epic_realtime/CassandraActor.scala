package com.epic_realtime
import akka.actor.{ Actor, Props, PoisonPill }
import scala.util.{Failure, Success}
import scala.concurrent.duration._
import scala.concurrent.{ExecutionContext, Future, Await}
import ExecutionContext.Implicits.global

import java.util
import java.util.Properties

import org.apache.kafka.clients.consumer.{ConsumerRecords, KafkaConsumer}
import org.apache.kafka.common.errors.WakeupException;
import scala.collection.JavaConverters._

import com.datastax.driver.core.{Session, Cluster, Host, Metadata}
import twitter4j.json.DataObjectFactory
import play.api.libs.json._ 
import com.typesafe.config.{ Config, ConfigFactory }



object CassandraActor {
  def props(name: String) = Props(new CassandraActor(name))
 
  case class SaveTweets(topic: String)
  case class StopConsumer(topic: String)

}


class CassandraActor(name: String) extends Actor {
  import CassandraActor._
  
  val config = ConfigFactory.load() 
  val kafka_server = config.getString("kafka.server") 
  val cassandra_host = config.getString("cassandra.host") 

  val pr = new Properties()
  pr.put("bootstrap.servers", kafka_server)
  pr.put("group.id", "test-consumer-group")
  pr.put("enable.auto.commit", "true")
  pr.put("auto.commit.interval.ms", "1000")
  pr.put("session.timeout.ms", "30000")
  pr.put("key.deserializer","org.apache.kafka.common.serialization.StringDeserializer")  
  pr.put("value.deserializer","org.apache.kafka.common.serialization.StringDeserializer")
  pr.put("fetch.min.bytes","50000")
  pr.put("receive.buffer.bytes","262144")
  pr.put("max.partition.fetch.bytes","2097152")
  pr.put("auto.offset.reset", "earliest")
  //https://kafka.apache.org/08/configuration.html

  val consumer = new KafkaConsumer[String, String](pr)

  def receive = {

    case SaveTweets(topic) =>
   
      // Connect directly to Cassandra from the driver
      val cluster = Cluster.builder().addContactPoint(cassandra_host).build()
      println(s"Connected to cluster: ${cluster.getMetadata().getClusterName()}")
      //.withCredentials(cassandra_user, cassandra_pass).build()
      val session = cluster.connect()
      consumer.subscribe(util.Arrays.asList(topic))
      println("A consumer is created for the topic " + topic)

      var count = 0l
      try {
          while (true) {
            val records: ConsumerRecords[String, String] = consumer.poll(100)
            println(s"${topic}: The consumer pulled ${records.count} messages.")
            Thread.sleep(50)
            for (r <- records.iterator().asScala) {
              //println(s" ${r.offset} ${r.key}")
              //println(s" ${r.value}")
              var data = r.value
              var data_status = DataObjectFactory.createStatus(data)         
              // println(data_status.getText())

              //Write to Cassandra
              session.execute(
                "INSERT INTO epic_realtime.tweets (event_name, tweet_id, created_at, tweet_json) " +
                "VALUES (" +
                    "'" + topic + "'," +
                    "'" + data_status.getId() + "'," +
                    "'" + data_status.getCreatedAt() + "'," +
                    "'" + data.toString.replace("'", "''") + "'" +
                ");")
              count += 1
            }
            consumer.commitAsync()
          }
      } catch {
         case e: Exception =>
            println("Error during processing of the message: " + e)          
      } finally {
          try {
              consumer.commitSync()
          } finally {
              consumer.close()
              session.close()
              cluster.close()
          }
      }


      case StopConsumer(topic) =>
        consumer.unsubscribe()
        consumer.close()
        println("\n\n\n-----------------------------------------\n\n\n")
        print(s"${topic}: The consumer is closed.")
        println("\n\n\n-----------------------------------------\n\n\n")


  }
}
// http://www.confluent.io/blog/tutorial-getting-started-with-the-new-apache-kafka-0.9-consumer-client
// https://www.safaribooksonline.com/library/view/kafka-the-definitive/9781491936153/ch04.html
// https://gist.github.com/hachikuji/35023fe424438253ada3
