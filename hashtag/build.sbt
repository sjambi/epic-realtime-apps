name := "epic_realtime"

version := "1.0"

scalaVersion := "2.10.5"

libraryDependencies ++= {
  val sparkVersion = "1.6.1"
  Seq(
   "org.apache.spark" %% "spark-core" % sparkVersion % "provided",
   "org.apache.spark" %% "spark-sql" % sparkVersion % "provided",
   "org.apache.spark" %% "spark-streaming" % sparkVersion % "provided",   
   ("com.datastax.spark" %% "spark-cassandra-connector" % "1.6.0").exclude("io.netty", "netty-handler"),
   ("org.apache.spark" %% "spark-streaming-kafka" % sparkVersion).exclude("org.spark-project.spark", "unused"),
   "com.typesafe.play" %% "play-json" % "2.4.0-M3",
   "com.github.nscala-time" %% "nscala-time" % "1.8.0",
   "com.databricks" % "spark-csv_2.10" % "1.4.0",
   "io.netty" % "netty-all" % "4.1.3.Final"
  )
}
