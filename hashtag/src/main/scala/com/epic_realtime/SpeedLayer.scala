package com.epic_realtime

import com.epic_realtime.SparkCommon._
// Spark Streaming + Kafka imports
import kafka.serializer.StringDecoder // this has to come before streaming.kafka import
import org.apache.spark.streaming._
import org.apache.spark.streaming.kafka._
import com.datastax.spark.connector._
import java.text.SimpleDateFormat
import java.sql.Timestamp 
import org.apache.spark.sql._
import org.apache.spark.sql.types._
import org.apache.spark.sql.functions._


object SpeedLayer {

  // def getHashtags(rdd: RDD[String, String]): RDD[String, String, String] = { 
  //   rdd.map( r => (r._1, r._2, "hi"))
  // }

  // This is a continuous query type
  def main(args: Array[String]) {
    if (args.length < 3) {
      System.err.println("Usage: com.epic_realtime.SpeedLayer <cores> <seconds> <event_name>")
      System.exit(1)
    }
    
    val cores = args(0)
    val seconds = args(1)
    val event = args(2) 
    val conf = SparkCommon.conf("SpeedLayer_hashtag", cores)
    val sc = SparkCommon.sc(conf)
    val ssc = SparkCommon.ssc(sc, seconds.toInt)
    
    // Kafka configuration
    val kafkaConf = SparkCommon.kafkaConf
    val kafka_topic = event
    val topicsSet = Set[String] (kafka_topic)

    val directKafkaStream = KafkaUtils.createDirectStream[String, String, StringDecoder, StringDecoder](
      ssc, kafkaConf, topicsSet).map(_._2)
    // directKafkaStream.cache()

    val sqlContext = new org.apache.spark.sql.SQLContext(sc)
    // this is used to implicitly convert an RDD to a DataFrame.
    import sqlContext.implicits._
    import org.apache.spark.sql._

    directKafkaStream.foreachRDD(rdd => {
      println("\nPopular topics in last 5 seconds (%s total):".format(rdd.count()))
      if (rdd.count>0){

      import java.util.TimeZone;  
      val sdf = new SimpleDateFormat("EEE MMM dd hh:mm:ss z yyyy")
      sdf.setTimeZone(TimeZone.getTimeZone("MDT"))

      val df = sqlContext.read.json(rdd)
      df.select(df("created_at"),df("text"))//.rdd
       .map{ case Row(created_at: String, text: String) => (created_at, text)
        }.flatMap( row => 
          row._2
          .replaceAll("[\\s.:;,!?&=<>()]", " ")
          .replaceAll("\\\\", " ")
          .replaceAll("\\/", " ")
          .replaceAll("\"", " ")
          .replaceAll("\'", " ")
          .split(" ").filter(_.startsWith("#")).map( hashtag =>
              (event,
              new java.sql.Timestamp(sdf.parse(row._1).getTime()),
              hashtag.stripSuffix("…")
              ))
          // Mon Aug 08 19:22:45 +0000 2016  =  EEE MMM dd hh:mm:ss z yyyy  =>  
          // 2016-10-27 16:51:51+0000  =  yyyy-MM-dd hh:mm:ssz
        ).saveToCassandra("epic_realtime", "hashtags_realtime", SomeColumns("event_name", "time", "hashtag"))
  
      // val df = sqlContext.read.json(rdd)
      // df.select(df("created_at"),df("text"))//.rdd
      //  .map{ case Row(created_at: String, text: String) => (created_at, text)
      //   }.map(getHashtags)
      //   .collect.foreach(println)

      }
    })

    ssc.start()
    // timer.start()
    ssc.awaitTermination()
    // ssc.stop()

  }

}
