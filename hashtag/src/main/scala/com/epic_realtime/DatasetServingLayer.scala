package com.epic_realtime

import com.epic_realtime.SparkCommon._
import com.datastax.spark.connector._
import org.apache.spark.sql._
import org.apache.spark.sql.SQLContext
import org.apache.spark.sql.types._
import org.apache.spark.sql.Row
import org.apache.spark.sql.functions._
import org.apache.spark.sql.types.{StructType,StructField,StringType};
import org.apache.spark.rdd.RDD
import org.apache.spark.streaming.dstream.ConstantInputDStream
import com.github.nscala_time.time.Imports._


object DatasetServingLayer {

  //--------------------------------
  // This is a continuous query type
  //--------------------------------
  def main(args: Array[String]) {
    if (args.length < 4) {
      System.err.println("Usage: com.epic_realtime.DatasetServingLayer <cores> <seconds> <event_name> <query_name> " +
        "<start_date[yyyy-MM-dd]> <end_date[yyyy-MM-dd]>")
      System.exit(1)
    }

    val cores = args(0)
    val seconds = args(1)
    val event = args(2)
    val query_name = args(3)
    val start_date = if (args.length > 4) args(4) else "" 
    val end_date = if (args.length > 5) args(5) else "" 
    var updated_start_date = ""
    var filter = ""

    val conf = SparkCommon.conf(query_name, cores)
    val sc = SparkCommon.sc(conf)
    val ssc = SparkCommon.ssc(sc, seconds.toInt)
    import java.util.Properties
    import org.apache.kafka.clients.producer.{ProducerRecord, KafkaProducer}
    val topic = query_name
    val producer = new KafkaProducer[String, String](SparkCommon.prop)

    // var realtimeRDD = sc.cassandraTable("epic_realtime", s"hashtags_realtime")
    // var batchRDD = sc.cassandraTable("epic_realtime", s"hashtags_batch")

    // if (start_date == ""){
    //   start_date2 = DateTime.now.toString
    // }else{
    //   start_date2 = start_date
    // }
    // if (end_date == ""){
    //   realtimeRDD = realtimeRDD.where("event_name = ? and time >= ?", event, start_date2)
    //   batchRDD = batchRDD.where("event_name = ? and time >= ?", event, start_date2)
    // }else{
    //   realtimeRDD = realtimeRDD.where("event_name = ? and time >= ? and time <= ?", event, start_date2, end_date)
    //   batchRDD = batchRDD.where("event_name = ? and time >= ? and time <= ?", event, start_date2, end_date)
    // }

    // Query Date Entry:
    // - Now           =>   start_date = "",          end_date = ""
    // - Since         =>   start_date = user_input,  end_date = ""
    // - Time Period   =>   start_date = user_input,  end_date = user_input
   
    if (end_date.isEmpty){
      //Now or Since
      updated_start_date = if (start_date.isEmpty) DateTime.now.toString else start_date
      filter = s"event_name = '${event}' and time >= '${updated_start_date}'"
    } else {
      //Time Period
      filter = s"event_name = '${event}' and time >= '${start_date}' and time <= '${end_date}'"
    }

    // Combine data from two Cassandra tables using union
    val realtimeRDD = sc.cassandraTable("epic_realtime", s"hashtags_realtime")
      .select("time", "hashtag").where(filter)
    val batchRDD = sc.cassandraTable("epic_realtime", s"hashtags_batch")
      .select("time", "hashtag").where(filter)
    val unionRDD = realtimeRDD.union(batchRDD)//.distinct
    // unionRDD.cache()

    // Generate continuous stream of Dstream
    val dstream = new ConstantInputDStream(ssc, unionRDD)

    dstream.foreachRDD{ rdd =>

      val d1 = DateTime.now

      if (rdd.count>0){

        val d1 = DateTime.now

        // Convert RDD to Data Frame objects
        val sqlContext = new SQLContext(sc)
        import sqlContext.implicits._

        // Generate the schema based on a string of schema
        val schemaString = "time hashtag"
        val schema = StructType(schemaString.split(" ").map(fieldName => StructField(fieldName, StringType, true)))
        // Convert records of RDD to Rows.
        val rowRDD = rdd.map(r => Row(r.get[String]("time"), r.get[String]("hashtag")))
        // Apply the schema to the RDD.
        val hashtagDF = sqlContext.createDataFrame(rowRDD, schema)
       // hashtagDF.printSchema()

        val hashtagCount = hashtagDF
          .select(col("hashtag"))
          .distinct
          .sort(asc("hashtag"))
        hashtagCount.show()

        val d2 = DateTime.now
        val dateDiff = d2.getMillis()-d1.getMillis()
        producer.send(new ProducerRecord[String, String]
          (topic, s"${realtimeRDD.count}, ${batchRDD.count}, ${rdd.count.toString}, ${dateDiff.toString}"))

        hashtagCount.write
          .format("org.apache.spark.sql.cassandra")
          .options(Map( "table" -> s"${query_name.toLowerCase()}", "keyspace" -> "epic_realtime"))
          .mode(SaveMode.Overwrite)
          .save()

        // if (start_date == ""){
        //   start_date2 = DateTime.now.toString
        // }else{
        //   start_date2 = start_date
        // }
        // Get a new time value for start_date in case of Now date entry
        if (end_date.isEmpty){
          //Now or Since
          updated_start_date = if (start_date.isEmpty) DateTime.now.toString else start_date
          filter = s"event_name = '${event}' and time >= '${updated_start_date}'"
        } else {
          //Time Period
          filter = s"event_name = '${event}' and time >= '${start_date}' and time <= '${end_date}'"
        }       


      }
    }

    ssc.start()
    // timer.start()
    ssc.awaitTermination()
    // ssc.stop()

  }
}

//http://stackoverflow.com/questions/32451614/reading-from-cassandra-using-spark-streaming
//https://databricks.com/blog/2015/09/16/apache-spark-1-5-dataframe-api-highlights.html
