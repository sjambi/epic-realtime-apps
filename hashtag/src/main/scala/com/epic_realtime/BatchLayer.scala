package com.epic_realtime

import com.epic_realtime.SparkCommon._
import com.datastax.spark.connector._
import com.datastax.spark.connector.cql.CassandraConnector
import play.api.libs.json._ 
import java.text.SimpleDateFormat
import java.sql.Timestamp 
import com.github.nscala_time.time.Imports._
import org.apache.spark.rdd.RDD

object BatchLayer {


  // def getHashtags(rdd: RDD[String, String]): RDD[String, String, String] = { 
  //   rdd.map( r => (r._1, r._2, "hi"))
  // }


 // This is a scheduled query type
  def main(args: Array[String]) {
    if (args.length < 2) {
      System.err.println("Usage: com.epic_realtime.BatchLayer <cores> <event_name>")
      System.exit(1)
    }

    val cores = args(0)
    val event = args(1)
    val conf = SparkCommon.conf("BatchLayer_hashtag", cores)
    val sc = SparkCommon.sc(conf)

    import java.util.Properties
    import org.apache.kafka.clients.producer.{ProducerRecord, KafkaProducer}
    val topic = "USE2016_trend_hashtag_01"//s"${event}_LA"
    val producer = new KafkaProducer[String, String](SparkCommon.prop)

    var count = 0
    while (true){

      val d1 = DateTime.now

      // Create a hashtag row view
      val tweetRDD = sc.cassandraTable("epic_realtime", "tweets")
        .select("created_at","tweet_json")
        .where("event_name = ?", event)
        // .spanBy(row => (row.getString("event_name"), row.getDate("created_at")))
      // tweetRDD.cache()
      // tweetRDD.partitioner

      tweetRDD.flatMap( row => 
        (Json.parse(row.getString("tweet_json")) \ "text").toString
        // .replaceAll("[^\\p{ASCII}]", " ")
        .replaceAll("[\\s.:;,!?&=<>()]", " ")
        .replaceAll("\\\\", " ")
        .replaceAll("\\/", " ")
        .replaceAll("\"", " ")
        .replaceAll("\'", " ")
        .split(" ").filter(_.startsWith("#"))
        .map(hashtag => 
          (event, 
          new java.sql.Timestamp(new SimpleDateFormat("EEE MMM dd hh:mm:ss z yyyy")
            .parse(row.getString("created_at")).getTime()),
          hashtag.stripSuffix("…")
          ))
      ).saveToCassandra("epic_realtime", "hashtags_batch", SomeColumns("event_name", "time", "hashtag")) 
   
      // tweetRDD.map(getHashtags)
      // .collect.foreach(println)
      // // .saveToCassandra("epic_realtime", "hashtags_batch", SomeColumns("event_name", "time", "hashtag"))  

      val d2 = DateTime.now
      val dateDiff = d2.getMillis()-d1.getMillis()
      val fmt = DateTimeFormat.forPattern("MMdd-HH:mm:ss")

      // producer.send(new ProducerRecord[String, String]
      //   (topic, s"Hashtag, ${d1.toString(fmt)}, ${tweetRDD.count.toString}, ${dateDiff.toString}"))

      // Add up realtime updates 
      sc.cassandraTable("epic_realtime", "hashtags_realtime").where("event_name = ?", event)
        .saveToCassandra("epic_realtime", "hashtags_batch", SomeColumns("event_name", "time", "hashtag"))

      // // Clean up all related realtime updates 
      CassandraConnector(conf).withSessionDo { session =>
        session.execute(s"DELETE FROM epic_realtime.hashtags_realtime WHERE event_name = '${event}'")
      }

      count += 1
      println("\n\n\n============================================\n")
      println(s"WHILE LOOP: COUNT = ${count}")
      println("\n============================================\n\n\n")

      producer.send(new ProducerRecord[String, String]
        (topic, s"Hashtag: Batch Count = ${count}, RDD COUNT = ${tweetRDD.count}, TIME = ${dateDiff}"))

    }//end while-loop
    //sc.stop()

  }
}

//https://github.com/datastax/spark-cassandra-connector

// import com.datastax.driver.core.{Session, Cluster, Host, Metadata}
  // val cassandra_host = "localhost"
  //     // connect directly to Cassandra from the driver to create the keyspace
  // val cluster = Cluster.builder().addContactPoint(cassandra_host).build()
  // println(s"Connected to cluster: ${cluster.getMetadata().getClusterName()}")
  // //.withCredentials(cassandra_user, cassandra_pass).build()
  // val session = cluster.connect()
  // val results = session.execute("SELECT * FROM epic_realtime.tweets " +
  //   "WHERE event_name = 'BFF2013';")
  // results.take(10).foreach(println)
 

