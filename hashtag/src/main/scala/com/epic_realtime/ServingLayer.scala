package com.epic_realtime

import com.epic_realtime.SparkCommon._
import com.datastax.spark.connector._
import org.apache.spark.sql._
import org.apache.spark.sql.SQLContext
import org.apache.spark.sql.types._
import org.apache.spark.sql.Row
import org.apache.spark.sql.functions._
import org.apache.spark.sql.types.{StructType,StructField,StringType};
import org.apache.spark.rdd.RDD
import org.apache.spark.streaming.dstream.ConstantInputDStream
import com.github.nscala_time.time.Imports._
import java.sql.Timestamp 


object ServingLayer {

  //--------------------------------
  // This is a continuous query type
  //--------------------------------
  def main(args: Array[String]) {
    if (args.length < 4) {
      System.err.println("Usage: com.epic_realtime.ServingLayer <cores> <seconds> <event_name> <query_name> " +
        "<trendtype[threshold/topk] <trendvalue> <start_date[yyyy-MM-dd]> <end_date[yyyy-MM-dd]>")
      System.exit(1)
    }
    
    val cores = args(0)
    val seconds = args(1)
    val event = args(2)
    val query_name = args(3)
    val trend = if (args.length > 4) args(4) else "topk"   //default value
    val value = if (args.length > 5) args(5) else "10"     //default value
    val start_date = if (args.length > 6) args(6) else "" 
    val end_date = if (args.length > 7) args(7) else "" 
    var updated_start_date = ""
    var filter = ""
 
    val conf = SparkCommon.conf(query_name, cores)
    val sc = SparkCommon.sc(conf)
    val ssc = SparkCommon.ssc(sc, seconds.toInt)
    import java.util.Properties
    import org.apache.kafka.clients.producer.{ProducerRecord, KafkaProducer}
    val topic = query_name
    val producer = new KafkaProducer[String, String](SparkCommon.prop)
    
    // Query Date Entry:
    // - Now           =>   start_date = "",          end_date = ""
    // - Since         =>   start_date = user_input,  end_date = ""
    // - Time Period   =>   start_date = user_input,  end_date = user_input
   
    if (end_date.isEmpty){
      //Now or Since
      updated_start_date = if (start_date.isEmpty) DateTime.now.toString else start_date
      filter = s"event_name = '${event}' and time >= '${updated_start_date}'"
    } else {
      //Time Period
      filter = s"event_name = '${event}' and time >= '${start_date}' and time <= '${end_date}'"
    }

    // Combine data from two Cassandra tables using union
    val realtimeRDD = sc.cassandraTable("epic_realtime", s"hashtags_realtime")
      .select("time", "hashtag").where(filter)
    val batchRDD = sc.cassandraTable("epic_realtime", s"hashtags_batch")
      .select("time", "hashtag").where(filter)

    val unionRDD = realtimeRDD.union(batchRDD)//.distinct
    unionRDD.cache()

    // Generate continuous stream of Dstream
    val dstream = new ConstantInputDStream(ssc, unionRDD)

    dstream.foreachRDD{ rdd =>

      if (rdd.count>0){
 
        producer.send(new ProducerRecord[String, String] (topic, filter))
        import java.text.SimpleDateFormat
        import java.util.TimeZone;
        val sdf = new SimpleDateFormat("EEE MMM dd hh:mm:ss z yyyy")
        sdf.setTimeZone(TimeZone.getTimeZone("MDT"))
        TimeZone.setDefault(TimeZone.getTimeZone("MDT"))


        val now1 = new java.util.Date()
        // val t1 = new java.sql.Timestamp(new java.util.Date().getTime())
        val t1 = new java.sql.Timestamp(now1.getTime())

        // Convert RDD to Data Frame objects
        val sqlContext = new SQLContext(sc)
        import sqlContext.implicits._

        // Generate the schema based on a string of schema
        val schemaString = "time hashtag"
        // val schema = StructType(schemaString.split(" ").map(fieldName => StructField(fieldName, StringType, true)))
        val schema = new StructType(Array(
          StructField("time", StringType, nullable=false),
          StructField("hashtag", StringType, nullable=false)
          ))
        // Convert records of RDD to Rows.
        val rowRDD = rdd.map(r => Row(r.get[String]("time"), r.get[String]("hashtag")))
        // Apply the schema to the RDD.
        val hashtagDF = sqlContext.createDataFrame(rowRDD, schema)
        // hashtagDF.printSchema()

        // import java.text.SimpleDateFormat
        // val tweet_time_str = "2016-10-27"
        val fmt2 = DateTimeFormat.forPattern("EEE MMM dd hh:mm:ss z yyyy")
        // String str = fmt.print(dt);

        // val fmt = DateTimeFormat.forPattern("yyyMMdd-HH:mm:ss")
        // val format = new java.text.SimpleDateFormat("EEE MMM dd hh:mm:ss z yyyy")

        // LocalDate date = LocalDate.now();
        // DateTimeFormatter fmt = DateTimeFormat.forPattern("d MMMM, yyyy");
        // String str = date.toString(fmt);

        val tweet_time_df = hashtagDF.select(col("time")).first().getString(0) //java.sql.Date
        //org.joda.time.DateTime cannot be cast to java.sql.Date
        // val dt = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss").parseDateTime(tweet_time_df);
        val tweet_time = Timestamp.valueOf(tweet_time_df.dropRight(5))
        // val t3 = new java.sql.Timestamp(sdf.parse(tweet_time_df.dropRight(5)).getTime())

        val format = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
        // val t3 = format.parse(tweet_time_df.dropRight(5))
        // val t3 = new java.sql.Timestamp(tweet_time.getTime())

        // val pattern = "MMM d HH:mm:ss Z yyyy"
        // val input = "Apr 10 18:31:45 +0000 2015"

        val pattern = "yyyy-MM-dd HH:mm:ssZ"
        // val input = "Apr 10 18:31:45 +0000 2015"
        val t3 = DateTime.parse(tweet_time_df, DateTimeFormat.forPattern(pattern))
        val d1 = DateTime.now


        // val tweet_t = new Timestamp(miliseconds)
          // .cast("timestamp")).first()
        // val tweet_time: DateTime = DateTime.parse("2016-10-27 23:43", fmt2) 
        // val tweet_time: DateTime =  DateTimeFormat.forPattern("EEE MMM dd hh:mm:ss z yyyy").parseDateTime(tweet_time_df)
        // val tweet_t = Timestamp.valueOf(tweet_time_df)
        // format.parse(tweet_time_str)
        // DateTime.parse(tweet_time_str) 



        val hashtagCount = hashtagDF
          .select("time", "hashtag")
          .distinct
          .groupBy("hashtag").count()
          .sort(desc("count"),asc("hashtag"))

        // Check user's input
        val hashtagTrend =
        trend match {
          case "threshold" => hashtagCount.where("count>="+value)
          case "topk" => hashtagCount.limit(value.toInt)
          case _ => hashtagCount.limit(10)
        }
        hashtagTrend.show()

        println(s"RDD COUNT = ${rdd.count}")

        val d2 = DateTime.now
        val now2 = new java.util.Date()
        // val d4 = new java.sql.Timestamp(new java.util.Date().getTime())
        val t2 = new java.sql.Timestamp(now2.getTime())

        // import java.util.Calendar
        // val d6 = Calendar.getInstance().getTime() //java.sql.Date
        val processingTime = (d2.getMillis()-d1.getMillis())/1000
        // val processingTime = t2.getTime() - t1.getTime()    
        // val immediacyTime =  (d2.getMillis() - t3.getTime())/1000  //(d4.getTime() - tweet_time.getTime())/1000 //seconds
        val immediacyTime =  (d2.getMillis() - t3.getMillis())/1000  //(d4.getTime() - tweet_time.getTime())/1000 //seconds

        // val immediacyTime = dt.getMillis() //- d6
        // d2.getMillis()-tweet_time.getMillis()
        val fmt = DateTimeFormat.forPattern("yyyMMdd-HH:mm:ss")
         
        val data = s"[${updated_start_date} - ${end_date}] ${realtimeRDD.count}, ${batchRDD.count}, ${rdd.count.toString}, ${processingTime.toString}, ${d2.getMillis()} - ${t3.getMillis()} = ${immediacyTime.toString}"
        producer.send(new ProducerRecord[String, String] (topic, data))

        hashtagTrend.write
          .format("org.apache.spark.sql.cassandra")
          .options(Map( "table" -> s"${query_name.toLowerCase()}", "keyspace" -> "epic_realtime"))
          .mode(SaveMode.Overwrite)
          .save()
       
        // Get a new time value for start_date in case of Now date entry
        if (end_date.isEmpty){
          //Now or Since
          updated_start_date = if (start_date.isEmpty) DateTime.now.toString else start_date
          filter = s"event_name = '${event}' and time >= '${updated_start_date}'"
        } else {
          //Time Period
          filter = s"event_name = '${event}' and time >= '${start_date}' and time <= '${end_date}'"
        }


      }
    }

    ssc.start()
    ssc.awaitTermination()
    // ssc.stop()

  }
}

//http://stackoverflow.com/questions/32451614/reading-from-cassandra-using-spark-streaming
//https://databricks.com/blog/2015/09/16/apache-spark-1-5-dataframe-api-highlights.html
