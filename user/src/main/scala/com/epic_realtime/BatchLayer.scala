package com.epic_realtime

import com.epic_realtime.SparkCommon._
import com.datastax.spark.connector._
import com.datastax.spark.connector.cql.CassandraConnector
import play.api.libs.json._ 
import java.text.SimpleDateFormat
import java.sql.Timestamp 


object BatchLayer {

 // This is a scheduled query type
  def main(args: Array[String]) {
    if (args.length == 0) {
      System.err.println("Usage: com.epic_realtime.BatchLayer <event_name>")
      System.exit(1)
    }

    val event = args(0)
    val conf = SparkCommon.conf("BatchLayer_user", "1")
    val sc = SparkCommon.sc(conf)

    var count = 0
    while (true){

      // Create a user row view
      val cassandraRow = sc.cassandraTable("epic_realtime", "tweets").where("event_name = ?", event)
      // cassandraRow.cache()

      val userRow = cassandraRow.collect.map{ 
        row => (event, 
                new Timestamp(new SimpleDateFormat("EEE MMM dd hh:mm:ss z yyyy").parse(row.getString("created_at")).getTime()), 
                (Json.parse(row.getString("tweet_json")) \ "user" \ "id_str").toString.replaceAll("\"", ""),
                (Json.parse(row.getString("tweet_json")) \ "user" \ "screen_name").toString.replaceAll("\"", "")
               )}
      val collection = sc.parallelize(userRow)
      collection.toArray().foreach(println)
      collection.saveToCassandra("epic_realtime", "users_batch", SomeColumns("event_name", "time", "user", "screen_name"))

      // Add up realtime updates 
      sc.cassandraTable("epic_realtime", "users_realtime").where("event_name = ?", event)
        .saveToCassandra("epic_realtime", "users_batch", SomeColumns("event_name", "time", "user", "screen_name"))

      // Clean up all related realtime updates 
      // CassandraConnector(conf).withSessionDo { session =>
      //   session.execute(s"DELETE FROM epic_realtime.users_realtime WHERE event_name = '${event}'")
      // }

      count += 1
      println("\n\n\n============================================\n")
      println(s"WHILE LOOP: COUNT = ${count}")
      println("\n============================================\n\n\n")

    }//end while-loop
    // sc.stop()

  }
}

//https://github.com/datastax/spark-cassandra-connector

// import com.datastax.driver.core.{Session, Cluster, Host, Metadata}
  // val cassandra_host = "localhost"
  //     // connect directly to Cassandra from the driver to create the keyspace
  // val cluster = Cluster.builder().addContactPoint(cassandra_host).build()
  // println(s"Connected to cluster: ${cluster.getMetadata().getClusterName()}")
  // //.withCredentials(cassandra_user, cassandra_pass).build()
  // val session = cluster.connect()
  // val results = session.execute("SELECT * FROM epic_realtime.tweets " +
  //   "WHERE event_name = 'BFF2013';")
  // results.take(10).foreach(println)
 

