package com.epic_realtime

import com.epic_realtime.SparkCommon._
// Spark Streaming + Kafka imports
import kafka.serializer.StringDecoder // this has to come before streaming.kafka import
import org.apache.spark.streaming._
import org.apache.spark.streaming.kafka._
import com.datastax.spark.connector._
import java.text.SimpleDateFormat
import java.sql.Timestamp 
import org.apache.spark.sql._
import org.apache.spark.sql.types._
import org.apache.spark.sql.functions._

object SpeedLayer {

  // This is a continuous query type
  def main(args: Array[String]) {
    if (args.length == 0) {
      System.err.println("Usage: com.epic_realtime.SpeedLayer <event_name>")
      System.exit(1)
    }
    
    val event = args(0) 
    val conf = SparkCommon.conf("SpeedLayer_user", "1")
    val sc = SparkCommon.sc(conf)
    val ssc = SparkCommon.ssc(sc, 5)
    // val timer = new Thread() {
    //   override def run() {
    //     Thread.sleep(1000 * 30)
    //     ssc.stop()
    //   }
    // }
    
    // Kafka configuration
    val kafkaConf = SparkCommon.kafkaConf
    val kafka_topic = event
    val topicsSet = Set[String] (kafka_topic)

    val directKafkaStream = KafkaUtils.createDirectStream[String, String, StringDecoder, StringDecoder](
      ssc, kafkaConf, topicsSet).map(_._2)
    // directKafkaStream.cache()

    val sqlContext = new org.apache.spark.sql.SQLContext(sc)
    // this is used to implicitly convert an RDD to a DataFrame.
    import sqlContext.implicits._
    import org.apache.spark.sql._

    directKafkaStream.foreachRDD(rdd => {
      println("\nPopular topics in last 5 seconds (%s total):".format(rdd.count()))
      if (rdd.count>0){
        // rdd.cache()
        val df = sqlContext.read.json(rdd)//.as[Tweet]
        // df.printSchema()
        val hashTagRow = df.select(df("created_at"), df("user.id_str"), df("user.screen_name")).rdd.map{
          row => (event,
                 new Timestamp(new SimpleDateFormat("EEE MMM dd hh:mm:ss z yyyy").parse(row.getString(0)).getTime()),
                 row.getString(1),
                 row.getString(2)
                 )}     
        hashTagRow.collect.foreach{ row =>
          val collection = sc.parallelize(Seq(row))
          collection.toArray().foreach(println)
          collection.saveToCassandra("epic_realtime", "users_realtime", SomeColumns("event_name", "time", "user", "screen_name"))
        }     
      }   
    })

    ssc.start()
    // timer.start()
    ssc.awaitTermination()
    // ssc.stop()

  }

}
