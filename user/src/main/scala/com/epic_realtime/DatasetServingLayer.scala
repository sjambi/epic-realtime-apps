package com.epic_realtime

import com.epic_realtime.SparkCommon._
import com.datastax.spark.connector._
import org.apache.spark.sql._
import org.apache.spark.sql.SQLContext
import org.apache.spark.sql.types._
import org.apache.spark.sql.Row
import org.apache.spark.sql.functions._
import org.apache.spark.sql.types.{StructType,StructField,StringType};
import org.apache.spark.rdd.RDD
import org.apache.spark.streaming.dstream.ConstantInputDStream
import com.github.nscala_time.time.Imports._


object DatasetServingLayer {

  //--------------------------------
  // This is a continuous query type
  //--------------------------------
  def main(args: Array[String]) {
   if (args.length < 2) {
      System.err.println("Usage: com.epic_realtime.DatasetServingLayer <event_name> <query_name> " +
        "<start_date[yyyy-MM-dd]> <end_date[yyyy-MM-dd]>")
      System.exit(1)
    }

    var event = args(0)
    var query_name = args(1)
    var start_date = if (args.length > 2) args(2) else "" 
    var end_date = if (args.length > 3) args(3) else "" 
    
    val conf = SparkCommon.conf(query_name, "1")
    val sc = SparkCommon.sc(conf)
    val ssc = SparkCommon.ssc(sc, 30)
    import java.util.Properties
    import org.apache.kafka.clients.producer.{ProducerRecord, KafkaProducer}
    val topic = query_name
    val producer = new KafkaProducer[String, String](SparkCommon.prop)
     
    // Combine data from two Cassandra tables using union
    val realtimeRDD = sc.cassandraTable("epic_realtime", s"users_realtime").where("event_name = ?", event)
    val batchRDD = sc.cassandraTable("epic_realtime", s"users_batch").where("event_name = ?", event)
    val unionRDD = batchRDD.union(realtimeRDD)
    // unionRDD.cache()

    // Generate continuous stream of Dstream
    val dstream = new ConstantInputDStream(ssc, unionRDD)

    dstream.foreachRDD{ rdd => 

      if (rdd.count>0){

        val d1 = DateTime.now

        // Convert RDD to Data Frame objects
        val sqlContext = new SQLContext(sc)
        import sqlContext.implicits._
       
      // Generate the schema based on a string of schema
        val schemaString = "time user screen_name"
        val schema = StructType(schemaString.split(" ").map(fieldName => StructField(fieldName, StringType, true)))
        // Convert records of RDD to Rows.
        val rowRDD = rdd.map(r => Row(r.get[String]("time"), r.get[String]("user"), r.get[String]("screen_name")))
        // Apply the schema to the RDD.
        val userDF = sqlContext.createDataFrame(rowRDD, schema)
       // userDF.printSchema()

        if (start_date == "") {start_date = DateTime.now.minusDays(1)toString}

        val userCount = 
        if (end_date == ""){
          userDF
          .filter(to_date(userDF("time")).gt(start_date))
          .select(col("screen_name").alias("user"))
          .distinct
          .sort(asc("user"))
     
        }else{
          userDF
          .filter(date_format(to_date(userDF("time")), "yyyy-MM-dd").between(start_date, end_date))
          .select(col("screen_name").alias("user"))
          .distinct
          .sort(asc("user"))
        }
        userCount.show()
        
        val d2 = DateTime.now
        val dateDiff = d2.getMillis()-d1.getMillis()
        producer.send(new ProducerRecord[String, String]
          (topic, s"${realtimeRDD.count}, ${batchRDD.count}, ${realtimeRDD.count+batchRDD.count}, ${unionRDD.count}, ${rdd.count.toString}, ${dateDiff.toString}"))

        userCount.write
          .format("org.apache.spark.sql.cassandra")
          .options(Map( "table" -> s"${query_name.toLowerCase()}", "keyspace" -> "epic_realtime"))
          .mode(SaveMode.Overwrite)
          .save()
      }
    }

    ssc.start()
    // timer.start()
    ssc.awaitTermination()
    // ssc.stop()

  }
}

//http://stackoverflow.com/questions/32451614/reading-from-cassandra-using-spark-streaming
//https://databricks.com/blog/2015/09/16/apache-spark-1-5-dataframe-api-highlights.html
