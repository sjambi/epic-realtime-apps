package com.epic_realtime

import org.apache.spark.{SparkConf, SparkContext}
import org.apache.spark.streaming.{StreamingContext, Seconds}

object SparkCommon {
 
    val spark_master = "spark://localhost:7077"
    val cassandra_host = "localhost"

    def conf(app_name: String, cores:String) : SparkConf = {
        new SparkConf()
            .setMaster(spark_master)
            .setAppName(app_name)
            .set("spark.cassandra.connection.host", cassandra_host)
            //.set("spark.cassandra.username", "cassandra") //Optional            
            //.set("spark.cassandra.password", "cassandra") //Optional
            .set("spark.cores.max", cores)
            .set("spark.driver.allowMultipleContexts", "true") 
            .set("spark.executor.memory", "4g")
            //.set("spark.executor.instances", "2")
            .set("spark.kryoserializer.buffer.max", "1g")//"500m")
            .set("spark.eventLog.enabled", "true")
            .set("spark.serializer", "org.apache.spark.serializer.KryoSerializer")
    }


    def sc(conf: SparkConf) : SparkContext = {
    	new SparkContext(conf)
    }

    def ssc(sc:SparkContext, sec: Int): StreamingContext = {
    	new StreamingContext(sc, Seconds(sec))
    }


    val kafka_broker = "localhost:9092"
    val zookeeper_server = "localhost:2181"
    val kafkaConf = Map(
      "metadata.broker.list" -> kafka_broker,
      "zookeeper.connect" -> zookeeper_server,
      "group.id" -> "test-consumer-group",
      "zookeeper.connection.timeout.ms" -> "1000",
      "auto.offset.reset" ->  "largest")

    import java.util.Properties
    import org.apache.kafka.clients.producer.{ProducerRecord, KafkaProducer}
    val prop = new Properties()   
    prop.put("bootstrap.servers", kafka_broker)
    prop.put("acks", "all")
    prop.put("retries", "0")
    prop.put("batch.size", "16384")
    prop.put("linger.ms", "0")
    prop.put("buffer.memory", "33554432")
    prop.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer")
    prop.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer")
}

